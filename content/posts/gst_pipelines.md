---
title: "A summary of a common gst test pipelines"
date: 2020-02-09T12:53:07Z
draft: false
tags: [gstreamer, pipelines, gst]
author: Tomasz Wisniewski (hesperos)
summary: A short summary of pipelines allowing to generate test content.
---

Common mp3 file:

    gst-launch-1.0 audiotestsrc num-buffers=44100 ! lamemp3enc bitrate=256 ! filesink location=test.mp3

Video only H264 MPEG-TS:

    gst-launch-1.0 mpegtsmux name=mux ! filesink location=test.ts videotestsrc num-buffers=250 ! video/x-raw,width=640,height=480,framerate=25/1 ! x264enc ! mux.sink_123

Audio (mono) & Video ac3/H264 MPEG-TS:

    gst-launch-1.0 mpegtsmux name=mux ! filesink location=test.ts videotestsrc num-buffers=250 ! video/x-raw,width=640,height=480,framerate=25/1 ! x264enc ! mux.sink_123 audiotestsrc num-buffers=250 ! avenc_ac3 bitrate=192000 ! audio/x-ac3 ! mux.sink_124

Video H264 mp4:

    gst-launch-1.0 qtmux name=mux ! filesink location=test.mp4 videotestsrc num-buffers=500 ! video/x-raw,width=1280,height=720,framerate=25/1 ! x264enc ! mux.video_123

Audio (mono) & Video ac3/H264 mp4:

    gst-launch-1.0 qtmux name=mux ! filesink location=test.mp4 videotestsrc num-buffers=500 ! video/x-raw,width=1280,height=720,framerate=25/1 ! x264enc ! mux.video_123 audiotestsrc num-buffers=500 ! avenc_ac3 bitrate=192000 ! mux.audio_124

Audio (mono) & Video aac(lc)/mpeg2video mpg:

    gst-launch-1.0 mpegpsmux name=mux ! filesink location=test.mpg videotestsrc num-buffers=250 ! video/x-raw,width=320,height=200,framerate=25/1 ! mpeg2enc format=3 ! video/mpeg,mpegversion=2 ! mux.sink_123 audiotestsrc num-buffers=250 ! faac bitrate=128 ! audio/mpeg,mpegversion=4,rate=22050,channels=1,base-profile=lc ! mux.sink_124
