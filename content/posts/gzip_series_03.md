---
title: "Writing gzip decompressor: Decoding blocks compressed with Fixed Huffman Tables"
date: 2019-07-20T17:31:26+01:00
draft: false
tags: [ 'gzip', 'golang', 'compression', 'huffman' ]
author: Tomasz Wisniewski (hesperos)
summary: This is a part of a series where I describe my attempts to implement a gzip decompressor from scratch in golang. This part focuses on decoding blocks compressed with Fixed Huffman tables.
---

This is an exciting part. For the first time, there'll be a chance to
read something from a compressed file. I'm going to focus on Fixed
Huffman tables entirely in this part. In fact, most of the code written
in this part will be re-used to approach the blocks compressed with
dynamic tables. The only difference between the two approaches is the
tables used. In the former case, the tables are given in the
[rfc1951.txt:3.2.6](https://www.ietf.org/rfc/rfc1951.txt). In the latter they
have to be read from the file and decoded prior to attempting to decode
any block data.

## Huffman tables

As mentioned, the tables are provided in the
[rfc1951.txt:3.2.6](https://www.ietf.org/rfc/rfc1951.txt). Here they are (_let's call it range representation_):

>
                   Lit Value    Bits        Codes
                   ---------    ----        -----
                     0 - 143     8          00110000 through
                                            10111111
                   144 - 255     9          110010000 through
                                            111111111
                   256 - 279     7          0000000 through
                                            0010111
                   280 - 287     8          11000000 through
                                            11000111

Actually, that's not entirely true. As mentioned in [part2]({{< ref "posts/gzip_series_02.md" >}}), the tables
are represented only by code lengths so, the actual representation (let's call it _list representation_) of the
tables would be: 8, repeated 144 times, 9 repeated 112 times, 7 repeated
24 times, and finally 8 repeated 8 times.

## Plumbing

Before I move forward, let's prepare some groundwork. First, some content
for the `GoGzReader.Read`:

```
func (r *GoGzReader) SetupMemberReader() (*Member, error) {
	member, err := NewMember(r.origReader, &r.backBuffer)
	if err != nil {
		return nil, err
	}
	r.currentMember = member
	r.memberReader = io.TeeReader(r.currentMember, &r.backBuffer)
	return member, nil
}

func (r *GoGzReader) Read(p []byte) (int, error) {
	if r.currentMember == nil {
		if _, err := r.SetupMemberReader(); err != nil {
			return 0, err
		}
	}
	return r.memberReader.Read(p)
}
```

So, what's going on here? I've added a `TeeReader` in order to collect
the decoded history in an instance of `BackBuffer`. Whenever a `Read`
operation is performed on `currentMember`, the read data will be copied
to the `BackBuffer` as well. That way, I've separated myself from user
buffers - I will always have my required look back history to perform
LZ77 lookups whenever required.

In order to read data member's blocks, let's extend the `Member` type:

```
type Member struct {
	Header *MemberHeader
	Footer *MemberFooter

	backBuffer BackBufferReaderWriter
	staging    bytes.Buffer
	bitReader  *BitReader
}

```

Here, I've got a reference to back buffer (the look-up may cross a block
boundary - to keep it safe, `GoGzReader` is the owner of the
`BackBuffer` but `Member` can access it). There's an instance of
`BitReader` of course and an instance of `staging` buffer. Now, why
another buffer? Well, whenever a request comes to read X amount of bytes
- it would be quite difficult to fulfill that request and maintain all
the state of block decoding. Instead of doing that, I'm going to
decompress the entire block into the `staging` buffer and fulfill the
`Read` requests using its contents. If there's not enough data in that
buffer, I'll proceed with another block. That adds more copying
operations to the entire pipeline - but this shouldn't really be a
problem now. Once there's some tangible working code, it will be easier
to approach any required optimisations.

That's the skeleton for the `Read` function:

```
func readUncompressedBlock() {

}

func readCompressedBlock() {

}

func (m *Member) Read(p []byte) (int, error) {
	for m.staging.Len() < len(p) {
		blockType, isFinal, err := readBlockHeader(m.bitReader)
		if err != nil {
			return 0, err
		}

		switch blockType {
		case BlockUncompressed:
			readUncompressedBlock()

		case BlockCompressedFixed:
			readCompressedBlock()

		case BlockCompressedDynamic:
			readCompressedBlock()
		}

		if isFinal {
			break
		}
	}

	return m.staging.Read(p)
}
```

## Input files

Fixed blocks (as I will refer to them from now onwards) can be found especially
in short files. If someone decides to compress a short string like "hello,
world", the resulting gzip file and its deflate data member will be most
likely comprised of a single fixed block. For test purposes, let's
prepare a file like that:

    $ echo -n "hello, world" >testdata/hw.txt
    $ gzip testdata/hw.txt

Let's create a little test for that file:

```
func TestIfFileContainsAFixedBlock(t *testing.T) {
	inputFh, err := os.Open("testdata/hw.txt.gz")
	if err != nil {
		t.Fatalf("unable to open test asset: %s\n", err)
	}
	defer inputFh.Close()

	backBuffer, err := NewBackBuffer(123)
	if err != nil {
		t.Fatalf("unable to instantiate backbuffer: %s\n", err)
	}

	member, err := NewMember(inputFh, backBuffer)
	if err != nil {
		t.Fatalf("error when reading member: %s\n", err)
	}

	blockType, isFinal, err := readBlockHeader(member.bitReader)
	if err != nil {
		t.Fatalf("error when reading block header: %s\n", err)
	}

	if blockType != BlockCompressedFixed {
		t.Fatalf("expected a unique fixed compressed block, got: %v\n", blockType)
	}

	if isFinal != true {
		t.Fatalf("expected a unique final block, got: %v\n", isFinal)
	}
}
```

Well, the first run, wasn't very successful. The test failed, indicating
a block of type 0x03? This is strange. After further invesigation I've
discovered that the culprit is the `NewMemberHeader` function which uses
buffered `io`. This consumes too much input (as it performs greedy
buffering). In order to fix that problem, I've abandoned `bufio`
entirely there.

After running the test again, it passed. Indeed, the file contains a
single fixed block. That's good!

In order to make the code generic so, it'll be possible to cover both
dynamic and fixed tables case with the same functionality, some
transformations on the fixed tables definitions are required. First let's
store the fixed tables as a set of ranges:

```
type FixedCodeSymbolRange struct {
	// Defines the first value of the symbol range
	StartSymbol uint16

	// Defines the number of symbols encoded with given code-length
	Symbols uint8

	// Length of code for the symbol range from the original alphabet
	CodeLength uint8
}

func getFixedCodeSymbolRanges() []FixedCodeSymbolRange {
	return []FixedCodeSymbolRange{
		{0, 144, 8},
		{144, 122, 9},
		{256, 24, 7},
		{280, 8, 8},
	}
}

```

Now a transformation from _range representation_ to _list representation_
is required. This can be easily achieved with the following function:

```

// Transforms a slice of CodeSymbol ranges to a slice, mapping a symbol from
// the original alphabet (index) to code's bit-length.
// codeLengths[ symbol ] = codeLength
func convertCodeSymbolRangesToCodeList(csRanges []FixedCodeSymbolRange) []uint8 {
	lastSymbol := csRanges[len(csRanges)-1].LastSymbol()
	codeLengths := make([]uint8, lastSymbol, lastSymbol)
	for _, v := range csRanges {
		for symbol := v.StartSymbol; symbol < v.LastSymbol(); symbol++ {
			codeLengths[symbol] = v.CodeLength
		}
	}
	return codeLengths
}
```

Now, it's possible to convert a convenient _range representation_ to
_list representation_ - which will be easier to use when building a
lookup table.

A lookup table? There'll be no tree? Well, actually no. The tree is
pretty inconvenient and would be much slower to use than a hash table
representation. It's much simpler to build a hash table that translates
Huffman codes directly to their associated symbols rather than to build
the tree and traverse it bit by bit whilst reading the input.

Okay, it's time to generate the actual Huffman codes. The algorithm to do that
is outlined in the [rfc1951.txt:3.2.2](https://www.ietf.org/rfc/rfc1951.txt).
It's just a matter of implementing it. First, let's create a type representing a
Huffman code:

```

type HuffmanCode struct {
	Code   uint16
	Length uint8
}
```

I will use this _tuple_ type as a key in the lookup table when decoding
input. The lookup table is of the following type:

    type HuffmanLookup map[HuffmanCode]Symbol

and here's the code performing the Huffman decoding:

```
func getMinMaxHuffmanCodeLength(hl HuffmanLookup) (int, int) {
	minLen, maxLen := 255, 0

	for c, _ := range hl {
		if int(c.Length) > maxLen {
			maxLen = int(c.Length)
		}

		if int(c.Length) < minLen {
			minLen = int(c.Length)
		}
	}
	return minLen, maxLen
}

func reverseBits(code uint64, n uint) uint64 {
	reversed := bits.Reverse64(code)
	return reversed >> (64 - n)
}

func matchHuffmanCode(b *BitReader, hl HuffmanLookup, minLen, maxLen int) (Symbol, error) {
	var symbol Symbol = 0
	var completeCode uint16 = 0
	var completeLen uint8 = 0
	toRead := uint(minLen)

	for completeLen <= uint8(maxLen) {
		code, err := b.ReadBits(toRead)
		if err != nil {
			return 0, err
		}
		code = reverseBits(code, toRead)
		completeCode <<= toRead
		completeCode |= uint16(code)
		completeLen += uint8(toRead)

		hc := HuffmanCode{completeCode, completeLen}
		if symbol, keyExists := hl[hc]; keyExists {
			return symbol, nil
		} else {
			toRead = 1
		}
	}

	return symbol, nil
}
```

First, I determine the minimum and maximum length of the Huffman codes
(in bits) from the given lookup table. After that, an attempt to decode
the input is performed. The first step is to read, at least, the number
of bits comprising the shortest possible code in the table. If there's no
match, one more bit is read and another lookup performed. This happens
until a match is found - and we've got the original symbol or until I
reach the maximum possible Huffman code length; in such case the input is
invalid and there's no code in the `HuffmanLookup` map associated with
any symbol.

One thing to underline here. As mentioned, the codes are prefixed codes,
which means that in order to assemble the full code bit by bit, the
whole operation happens effectively in reversed order. The data that
is read from the stream needs to be reversed (if it contains more
than one bit).

Here's a revised version of `Member's` `Read` function:

```
func (m *Member) readCompressedBlock(hl HuffmanLookup) error {
	minLen, maxLen := getMinMaxHuffmanCodeLength(hl)
	isEndOfBlock := false

	for !isEndOfBlock {
		symbol, err := matchHuffmanCode(m.bitReader, hl, minLen, maxLen)
		if err != nil {
			return err
		}

		switch {
		case symbol < 256:
			m.staging.WriteByte(uint8(symbol))

		case symbol == 256:
			isEndOfBlock = true

		default:
		}
	}

	return nil
}

func (m *Member) Read(p []byte) (int, error) {
	for m.staging.Len() < len(p) {
		blockType, isFinal, err := readBlockHeader(m.bitReader)
		if err != nil {
			return 0, err
		}

		switch blockType {
		case BlockUncompressed:
			m.readUncompressedBlock()

		case BlockCompressedFixed:
			codeList := convertCodeSymbolRangesToCodeList(
				getFixedCodeSymbolRanges())
			lookup := GeneratHuffmanLookupFromCodeLengthList(codeList)
			m.readCompressedBlock(lookup)

		case BlockCompressedDynamic:
		}

		if isFinal {
			footer, err := NewMemberFooter(m.bitReader)
			if err != nil {
				return 0, err
			}
			m.Footer = footer
			break
		}
	}

	return m.staging.Read(p)
}
```

In order to verify if all of that works, let's add a little test:

```
func TestIfItsPossibleToReadSimpleFixedBlocks(t *testing.T) {
	inputFh, err := os.Open("testdata/hw.txt.gz")
	if err != nil {
		t.Fatalf("unable to open test asset: %s\n", err)
	}
	defer inputFh.Close()

	backBuffer, err := NewBackBuffer(123)
	if err != nil {
		t.Fatalf("unable to instantiate backbuffer: %s\n", err)
	}

	member, err := NewMember(inputFh, backBuffer)
	if err != nil {
		t.Fatalf("error when reading member: %s\n", err)
	}

	expected := "hello, world"
	got := make([]byte, len(expected)*2)

	n, err := member.Read(got)
	if err != nil {
		t.Fatalf("unexpected error: %v\n", err)
	}

	if n != len(expected) {
		t.Fatalf("got length: %d, expected: %d\n", n, len(expected))
	}

	gotStr := strings.TrimRight(string(got), "\x00")
	if gotStr != expected {
		t.Fatalf("got: [%s], expected: [%s]\n", gotStr, expected)
	}
}
```

The test... passes. That's the first step completed. This small package
is capable of reading very simple gzip files. There's a lot stuff missing
though. Here's a short list:

- `readCompressedBlock` function doesn't deal at all with LZ77 compression;
- the data written to the output is not verified against _crc32_ checksum
present in the footer.

I'll address the above in the next part of the series.

# Summary

All the code from this part in its
entirery can be found [here](https://gitlab.com/hesperos/gogz/tree/devdiary/part3).
