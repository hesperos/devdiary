---
title: "Type loops"
date: 2015-08-12T15:18:33+01:00
draft: false
tags: [ "c++", "templates" ]
author: Tomasz Wisniewski (hesperos)
summary: A reference for implementing a simple type loop
---

This is a neat little trick which I discovered some time ago

```
template <typename F, typename ... A>
void for_each(F f, A&&... args) {
    (void)(int[]) { (f(std::forward<A>(args)),0)...  };
}


```

Usage example:


```
#include <iostream>

template <typename F, typename ... A>
void for_each(F f, A&& ... args) {
    (void)(int[]){ (f(std::forward<A>(args)), 0)... };
}

template <typename T>
void printer(const T& t) {
    std::cout << "[" << t << "]" << std::endl;
}

int main(int argc, char *argv[])
{
    // usage with function template
    for_each(printer<int>, 1,2,3,4);

    // usage with lambda
    for_each([](int i){ std::cout << i << std::endl; }, 1,2,3,4);

    return 0;
}
```
