---
title: "Stream iterators"
date: 2014-11-27T17:09:44+01:00
draft: false
tags: ["c++","iterators"]
author: Tomasz Wisniewski (hesperos)
summary: Summary of stream iterators in c++
---

According to the
[cppreference#`istream_iterator`](https://en.cppreference.com/w/cpp/iterator/istream_iterator)
and
[cppreference#`istreambuf_iterator`](https://en.cppreference.com/w/cpp/iterator/istreambuf_iterator):

- `istream_iterator` - ...  reads successive objects of type T from the
`std::basic_istream` object ...

- `istreambuf_iterator` - ... reads successive characters from the
`std::basic_streambuf` object ...



# `stream_iterator`

Let's consider the following:

```
std::istream_iterator<int> si(std::cin);

std::cout << "read number: " << *si << std::endl;
```

This will fetch the input from console input stream and parse the integers out
of it, something like C's atoi(). The same can be done with strings, first they
need to be converted to stream though:


```
std::string s("123");
std::stringstream ss(s);
```


... since at this point `ss` is a stream



```
int n = 0;
ss >> n;
```


... will convert s "123" to int 123. The same can be achieved though the
iterator, the advantage is that it's possible to perform subsequent
conversions:



```
std::string s("123 456");
std::stringstream ss(s);
std::istream_iterator<int> si(ss);


while (si != std::istream_iterator<int>()) {
 std::cout << "read number: " << *si++ << std::endl;
}
```


By default `std::istream_iterator<T>()` constructs an iterator pointing to an
end() - that's why it's possible to compare current position with this
temporary variable. Of course such trivial task should be rather performed by a
library function like `std::strtoul`.


This has a lot of practical use cases, i.e. having a text file containing a
number in each line it's possible to read it into vector using `stream_iterator`
very easily:


```
std::vector<int> v;
std::ifstream ifs("file.dat");

std::copy(std::istream_iterator<int>(ifs),
  std::istream_iterator<int>(),
  std::back_inserter(v));
```

The `ostream_iterator` can be used to perform a formatted output, using the same method. Consider having a container:


```
std::vector<int> v { 1, 2, 3, 4 };
std::ofstream ofs("file.dat");
std::copy(v.begin(), v.end(), std::ostream_iterator<int>(ofs, "\n"));
ofs.close();
```

It's that easy! We don't have to use a file as output. The `cout` stream is as
good:


	std::copy(v.begin(), v.end(), std::ostream_iterator<int>(std::cout, "\n"));


Stream iterators can work the opposite way on strings as well. We can perform
formatted reading into a string using `ostream_iterator` as well. It doesn't
matter as long as the underlying output is the stream:


```
std::vector<int> v {1,2,3,4};
std::string output;
std::stringstream os;

std::copy(v.begin(), v.end(),
  std::ostream_iterator<int>(os, ", "));

std::cout << os.str() << std::endl;
```


# `streambuf_iterator`
`streambuf_iterator` is slightly different. It is more basic. It provides RAW
character based access, when used along with console or file streams it must be
instantiated with either char datatype or wchar_t since most of the time it
works with `basic_streambuf<char>` or `basic_streambuf<wchar_t>` types.

It works best when trying to perform raw access or iteration over the streams,
like so:

```
std::istringstream s("some string");

std::vector<char> v(
  (std::istreambuf_iterator<char>(s)),
  std::istreambuf_iterator<char>());


std::copy(v.begin(), v.end(),
  std::ostream_iterator<char>(std::cout, ", "));
```

... will produce:



	s, o, m, e,  , s, t, r, i, n, g,


It can be used as well to copy two streams, the following way:

```
std::ifstream ifs("a.dat");
std::ofstream ofs("b.dat");


std::copy(std::istreambuf_iterator<char>(ifs),
  std::istreambuf_iterator<char>(),
  std::ostreambuf_iterator<char>(ofs));
```
