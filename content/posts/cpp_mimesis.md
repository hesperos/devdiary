---
title: "Making lambda-unable algorithms lambda-able with object mimesis"
date: 2015-05-04T14:46:44+01:00
draft: false
tags: ["c++", "mimesis"]
author: Tomasz Wisniewski (hesperos)
summary: Detail on implementing object mimesis.
---

This is a clever trick which is very handy whenever there's an algorithm
expecting a value but instead we would like to pass it a lambda acting upon
that value in some way. A perfect example is `std::iota()`. It operates on a
given range and increments a given initial value in order to populate the
range. I.e.


```
std::vector v(16);
std::iota(v.begin(), v.end(), 1);
```

What if we would want to use iota to fill the vector with powers of two ?
Current interface does not allow that. We would need to use a lambda instead of
a static value. This is a situation where "mimesis" comes in place. It has been
very well described here: [advanced cpp
metaprogramming](http://acppmp.blogspot.it/). The trick is to define an object
overriding the conversion operator and in case of iota the increment operator.
Let's have a look on the code:


```
template <typename T>
struct square_mimesis {
    T x;

    operator T() const {
        return x;
    }

    void operator++() {
        x = 2*x;
    }
};


int main(int argc, char *argv[])
{
    std::vector<unsigned> v(16);
    std::iota(v.begin(), v.end(), square_mimesis<unsigned>{2});

    return 0;
}
```



This code is all-right but it would be good to use the lambda in place, the
second approach would be something like that:


```
#include <algorithm>
#include <vector>

template <typename T, typename F>
struct iota_mimesis_t {
    T x;
    F f;

    operator T() const {
        return x;
    }

    void operator++() {
        x = f(x);
    }
};

template <typename T, typename F>
iota_mimesis_t<T, F> iota_mimesis(const T& t, F f) {
    return iota_mimesis_t<T, F>{t, f};
}

int main(int argc, char *argv[])
{
    std::vector<unsigned> v(16);
    std::iota(v.begin(), v.end(), iota_mimesis(2, [](const unsigned& i){ return 2*i; }));

    return 0;
}
```

