---
title: "Static string obfuscation during compilation"
date: 2015-02-24T17:58:13Z
draft: false
tags: ["obfuscation", "mangling", "perl"]
author: Tomasz Wisniewski (hesperos)
summary: This is a very simple and crude approach that can be used as an example on how to hide sensitive data within executables.
---

This is my way of building a simple string obfuscation system to protect
vulnerable data embedded in the source code. In order to explain the details I
need to provide some background.

## The Problem
Imagine that you're writing a client application which will be deployed on some
systems. Your application will periodically contact a central server to report
some data. Obviously, such application must know the credentials to the server
in order to establish the session. A generic code would look like so:

    connect_to_server("login", "password");

This is all fine. But once you compile that and examine the executable it comes
out that the precious credentials are all there on a plate:

```
$ objdump -j .rodata a.out -s a.out

a.out:     file format elf32-i386

Contents of section .rodata:
 8048468 03000000 01000200 70617373 776f7264  ........password
 8048478 006c6f67 696e00                      .login.
```

or with `readelf`:

```
$ readelf -p .rodata a.out

String dump of section '.rodata':
  [     8]  password
  [    11]  login
```

or even with the simplest `strings` command:

```
$ strings a.out
/lib/ld-linux.so.2
libc.so.6
_IO_stdin_used
__libc_start_main
__gmon_start__
GLIBC_2.0
PTRhP
t$,U
[^_]
password
login
;*2$"
GCC: (GNU) 4.9.2 20150204 (prerelease)
```


That's not good. From the system design point of view this is a security risk.
Even without the sources exposed to the public anyone can get the vulnerable
data and attack the server. Other approach would be to store the password in
the external file. But that's not good either, since the file would have to be
delivered with the software itself, so the credentials would be even more
exposed to the potential attacker.


Unfortunately there is no easy way to solve this problem. The strings must be
obfuscated at compilation time before they end up in the resulting executable.


## The Solution
I had to solve a similar challenge and introduce it to already existing system.
What I've come up with may sound over complicated but it's simple in general
(if you drop some of the technical details needed to make it work). I decided
to pre-process a source file with a script which will replace all the marked
string declarations with their obscured versions. The executable itself during
the runtime must be able to decode the obscured versions on the fly in order to
use the original data.

Since I decided to use a single library and responsible for string encoding and
obfuscation and not duplicate the code in the obfuscation script and the
library itself it was most challenging to make those to work together.


The strings are encoded using the XTEA cipher. The output of the algorithm is
serialized afterwards (since not every value of the byte are ASCII printable,
the binary data varying from 0 - 255 is downcoded 6 bit range with some
offset). The XTEA cipher requires a key as well, but that's not a problem since
the key can be obtained from an external file or can be specific to a system
(like a system serial number or something similar which is unique and doesn't
change during the lifetime of the product).


### Components
Here's what's involved.

**libmangle** - I created a special static library which contains an implementation
of XTEA cipher (encryption/decryption routines) and serialization code (8 bit
values down to 6 bit values) `es.pl` - a pre-processing script. It is called for
all the concerned source files.

In bullets, the solution works like so:

#### Compilation phase
- Pre-process all the needed files with a script (`es.pl`)
For every file, lookup for specially marked declarations (`_ES_DECL`)
Replace those declarations with an obfuscated versions
Link the executable against the encryption library (`libmangle`)
Replace `_ES_DEC` macros with calls to `libmangle` decoding functions

#### Runtime:
- Obtain the key from the system (system id, serial number whatever)
Set the key using libmangle API
Use libmangle API decoding routines to decode the obfuscated strings and obtain
the original data.

##### libmangle
I wrote a set of functions and collected them into a static library which I
link to the executable. More information about the library can be obtained
[here](http://gitlab.com/hesperos/libmangle/)

##### `es.pl`
This script pre-processes the source files and generates source files with
obfuscated strings. Here's an example. Assume that you have the following file:
(`foo.c`)


```
#include "foo.h"
#include "es.h"

#include <stdio.h>

void hello(void) {
    _ES_DECL(hello_msg, "This is a hello message in file foo.c");
    printf("%s(): %s\n", __FUNCTION__, _ES_DEC(hello_msg));
}
```

`es.pl` looks for `_ES_DECL` declarations. Once it founds one it'll extract the
static string from it encode it and replace with the encoded version. This
invocation:

	./es.pl -k secret.txt foo.c

will in result produce `foo_es.c` which should look something like the that:


```
#include "foo.h"
#include "es.h"

#include <stdio.h>


void hello(void) {
    char hello_msg[68] = { "`lX>N>4idNBWmDBekYY[oT>fJJb8C^;IM:?M8?Szognb8c99Bd=3G522" };
    printf("%s(): %s\n", __FUNCTION__, _ES_DEC(hello_msg));
}
```


This file is almost identical to the original the only difference is the
`hello_msg` string declaration which is obfuscated. Such file can be safely
compiled since all the sensitive data is hidden.

## Example
Let's try to consider a complete example now. First of all `libmangle` is required:

	git clone https://gitlab.com/hesperos/libmangle

Let's create two source files now: `foo.c` and `test.c` and a header `es.h`
containing some pre-processor definitions giving us some flexibility. This is
how it looks:


```
#ifndef ES_H_MTO1JT8W
#define ES_H_MTO1JT8W


/**
 * declare obfuscated string.
 * The Perl pre-processor should:
 *  encode and serialize the string
 *  do a proper declaration with size adjusted to the xtea blocks
 *
 */
#define _ES_DECL(__var, __string) char __var[] = { __string }

#if _ES_ENABLED == 1


#include "libmangle/mangle.h"


/**
 * this is an interface for C application
 */

#define _ES_DEC(__string) mangle_xtea_dec(__string, xtea_default_key)
#define _ES_DEC_CONST(__string) mangle_xtea_dec_const(__string, xtea_default_key)
#define _ES_GC() mangle_gc()

#else

#define _ES_DEC(__string) __string
#define _ES_DEC_CONST(__string) __string
#define _ES_GC()

#endif

#endif /* end of include guard: ES_H_MTO1JT8W */
```

The trick is that we can statically decide whether a string obfuscation is
needed or not with `_ES_ENABLED` macro. If it's value is 0, the `_ES_DECL` simply
declares the strings as they are. The same applies to `_ES_DEC` macros. They will
be replaced with the strings themselves as they are. If the `_ES_ENABLED` macro's
value is 1, the `_ES_DECL` will be dynamically replaced in the pre-processing
stage by the `es.pl` script and the `_ES_DEC` calls will include the decoding
routines from `libmangle` in order to obtain the original strings in runtime. How
we can easily control the value of the `_ES_ENABLED` macro ? This will be defined
dynamically during the compilation in the `Makefile`. Let's have a look on the
Makefile as well:

```
TARGET=encrypted_strings_test
SECRET=secret.txt
CSRC= \
    test.c \
    foo.c

ES_SRC=$(CSRC:.c=_es.c)
ES_OBJ=$(ES_SRC:.c=.o)

OBJ=$(ES_OBJ)

CC=gcc
CFLAGS=-I. -Wall -D_ES_ENABLED=1
LDFLAGS=-lmangle -Llibmangle/
LIBMANGLE=libmangle.a

all: $(TARGET)
.PHONY: clean $(LIBMANGLE) $(GENDIR)
.PRECIOUS: $(ES_SRC)

%_es.c: %.c
    @echo -e "\tGEN" $@ $<
    @./es.pl -v -k $(SECRET) $<


%_es.o: %_es.c
    @echo -e "\tCC" $@ $<
    @$(CC) -c -o $@ $< $(CFLAGS)

$(LIBMANGLE):
    $(MAKE) -C libmangle/

$(TARGET): $(LIBMANGLE) $(OBJ)
    @echo -e "\tLD" $(TARGET)
    @$(CC) -o $(TARGET) $(OBJ) $(LDFLAGS)

clean:
    $(MAKE) -C libmangle/ clean
    rm -rf _Inline/ *.s *.o $(TARGET)
```

Let's track what's going on backwards. In order to build the target, `libmangle`
must be compiled and the `$(OBJ)` must be created. The `OBJ` variable holds the
object file names of the obfuscated sources `$(ES_OBJ)` in our case those will be
`test_es.o` and `foo_es.o`. Those files will be generated by the compiler from
`test_es.c` and `foo_es.c` and those two sources will be created by `es.pl` from
`test.c` and `foo.c` respectively. `es.pl` needs a key file in our case to make
things simpler I'll just put a random string encode it with `base64` to a
`secret.txt` file:

	$ echo "This phrase is used to generate base64 hash." | base64 > secret.txt

Everything is ready. Let's have a look on test.c now.

```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "es.h"
#include "foo.h"


#if _ES_ENABLED == 1
void configure_key() {
    FILE *f = fopen("secret.txt", "r");
    xtea_key_t key;

    if (!f) {
        perror("unable to open secret.txt");
        return;
    }

    fread(key, sizeof(key), 1, f);
    fclose(f);
    mangle_xtea_set_key(key);
}
#endif


int main(int argc, char *argv[]) {
    _ES_DECL(s1, "this string will be encrypted on the fly");

#if _ES_ENABLED == 1
    configure_key();
#endif

    printf("%s\n", _ES_DEC(s1));
    hello();

    _ES_GC();
    return 0;
}
```

In order to be able to decode the strings, the runtime must read in the key.
This is done by the `configure_key()` routine if the encryption is enabled if not
this function is redundant and is silently removed by the preprocessor. Now
it's possible to run make, but before that, let's have a look on the project
file listing:

```
total 32
-rw------- 1 tomasz users  651 01-05 13:39 Makefile
-rw------- 1 tomasz users  733 12-19 17:21 es.h
-rwx--x--x 1 tomasz users 3076 01-05 13:39 es.pl
-rw------- 1 tomasz users  196 12-19 17:21 foo.c
-rw------- 1 tomasz users  116 12-18 16:41 foo.h
drwxr-xr-x 4 tomasz users 4096 02-24 21:08 libmangle
-rw------- 1 tomasz users   61 12-17 16:50 secret.txt
-rw------- 1 tomasz users  547 12-19 17:21 test.c
```

After make:

```
total 64
-rw------- 1 tomasz users  651 01-05 13:39 Makefile
drwxr-xr-x 4 tomasz users 4096 02-24 21:09 _Inline
-rwxr-xr-x 1 tomasz users 9892 02-24 21:09 encrypted_strings_test
-rw------- 1 tomasz users  733 12-19 17:21 es.h
-rwx--x--x 1 tomasz users 3076 01-05 13:39 es.pl
-rw------- 1 tomasz users  196 12-19 17:21 foo.c
-rw------- 1 tomasz users  116 12-18 16:41 foo.h
-rw-r--r-- 1 tomasz users  219 02-24 21:09 foo_es.c
-rw-r--r-- 1 tomasz users 1300 02-24 21:09 foo_es.o
drwxr-xr-x 4 tomasz users 4096 02-24 21:09 libmangle
-rw------- 1 tomasz users   61 12-17 16:50 secret.txt
-rw------- 1 tomasz users  547 12-19 17:21 test.c
-rw-r--r-- 1 tomasz users  579 02-24 21:09 test_es.c
-rw-r--r-- 1 tomasz users 1784 02-24 21:09 test_es.o
```

As mentioned before we've got some new files: `foo_es.c` `test_es.c` and their
derived compiled versions. We have also an `_Inline` directory. This one is being
created by the Perl's `Inline::C` module which compiles the C routines to Perl so
they can be used inside the script. This is done in order to avoid any
unnecessary code duplication (libmangle would have to be reimplemented in Perl
as well).

We can now test the final result:

```
$ ./encrypted_strings_test
this string will be encrypted on the fly
hello(): This is a hello message in file foo.c
```

The functionality is retained, but the strings in the executable are obfuscated:

```
$ readelf -p .rodata encrypted_strings_test

String dump of section '.rodata':
  [     8]  r
  [     a]  unable to open secret.txt
  [    24]  Qe_fV5a6SgaI>g6hWP=cgQTaKhGL=NJZJpCZaCI??Cg7f59RjJIZi@i@BcO@NbHO2222
  [    70]  `lX>N>4idNBWmDBekYY[oT>fJJb8C^;IM:?M8?Szognb8c99Bd=3G522
  [    b4]  %s(): %s^J
  [    be]  hello
  [    c4]  %s(): no memory
  [    d4]  _node_append
```

To disable the on-the-fly encryption let's set `_ES_ENABLED=0` in the Makefile
and change the `$(OBJ)` variable to contain the original files:

	OBJ=$(CSRC:.c=.o)

After recompilation, the `.rodata` section again contains the original strings:


```
$ readelf -p .rodata encrypted_strings_test

String dump of section '.rodata':
  [     8]  this string will be encrypted on the fly
  [    31]  This is a hello message in file foo.c
  [    57]  %s(): %s^J
  [    61]  hello
```

As usual, the complete set of sources for the test project can be found on
gitlab [dyn_strs](https://gitlab.com/hesperos/dyn_strs)
