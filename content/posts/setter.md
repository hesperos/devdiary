---
title: "Configuring objects with 'Settings closures'"
date: 2019-08-05T15:40:18+01:00
draft: false
tags: ["golang"]
author: Tomasz Wisniewski (hesperos)
summary: This post contains a description of an interesting technique used to configure objects.
---

There's an incredibly cool pattern which I've recently learned about. It's
quite useful when having to update values on on an object. Consider the
following:

```
type Led struct {
    name string
    brightness int
    r,g,b int
}
```

This is a simple object modelling an LED light. There are some setting that can
be altered and in practice, they would modify the settings of the physical
device that the object abstracts. The classic approach would handle settings
like that would be to add a set of functions:

```
func (l *Led) SetName(name string) {
    l.name = name
}

func (l *Led) SetRgb(r,g,b int) {
    l.r, l.g, l.b = r,g,b
}

func (l *Lef) SetBrightness(brightness int) {
    l.brightness = brightness
}
```

This would definitely work. But, this can be done better. `SetRgb()` always
alters all 3 colour variables at once. Of course, this function can be split
into 3 separate setter functions in order to model the abstraction on a
fine-grained level. There's a way to model settings in a more non-conventional
way:


```
type LedSetting interface {
    Set(l *Led)
}

type ledSetting func(l *Led)


func (ls ledSetting) Set(l *Led) {
    ls(l)
}
```

`ledSettings` implements `LedSetting` interface, the `Set` function simply
forwards the `Led` object to the `ledSetting` function. This can be used in a
following way:


```
func SetRgb(r,g,b int) LedSetting {
    return ledSetting(func(l *Led){
        l.r = r
        l.g = g
        l.b = b
    })
}

func SetBrightness(brightness int) LedSetting {
    return ledSetting(func(l *Led){
        l.brightness = brightness
    })
}

func SetName(name string) LedSetting {
    return ledSetting(func(l *Led){
        l.name = name
    })
}

```

These functions create closures which contain the desired value. These function
take a pointer to an object on which the actual configuration will be applied.
Let's extend the `Led` object with an `Apply` method:

```
func (l *Led) Apply(settings ...LedSetting) {
    for _, s := range settings {
        s.Set(l)
    }
}
```

Having all of that, the pattern is complete. It can be used the following way:

```
l := Led{}
l.Apply(SetBrightness(123), SetName("led01"), SetRgb(1,2,3))

```

The list can be arbitrarily long and contain any combination of settings which
allows for plenty of flexibility - especially when the possible combination set
is quite large.
