---
title: "Getting started with autotools"
date: 2014-01-12T16:33:27Z
draft: false
tags: ["autotools", "automake", "gcc", "configure"]
author: Tomasz Wisniewski (hesperos)
summary: A summary of workflow to prepare an autotools/automake project.
---

Here's a small workflow example for creating an autotools based project. First
let's start by creating the project directory. This will be a trivial project
build up of one file:

	$ mkdir hw && cd $_
	$ touch hw.c


The program itself doesn't really matter. I'll put something obvious:


	cat > hw.c
	#include <stdio.h>

	int main(int argc, char *argv[])

	{
	   printf("Hello, World\n");
	   return 0;
	}
	EOF

OK. Let's proceed with creating the autotools environment.



## autoconf
In order to generate the configure script which will query the system before
the compilation and generate a set of Makefiles using automake we need a
configure.ac input file. This can be automatically generated using autoscan
tool:

before:

	$ ls
	hw.c

after:

	$ autoscan
	$ ls
	autoscan.log  configure.scan  hw.c

The configure.scan can be renamed to configure.ac. It needs some small
adjustments:

	$ cat configure.ac
	#                                               -*- Autoconf -*-
	# Process this file with autoconf to produce a configure script.


	AC_PREREQ([2.69])
	AC_INIT([hello_world], [0.1], [hw@hw.com])
	AC_CONFIG_SRCDIR([.])
	AC_CONFIG_HEADERS([config.h])

	# Checks for programs.
	AC_PROG_CC

	# Checks for libraries.

	# Checks for header files.

	# Checks for typedefs, structures, and compiler characteristics.

	# Checks for library functions.

	AC_OUTPUT

We see that the `configure.ac` refers to `config.h` This file will be created from
`config.h.in`. Which as well can be generated:

	$ autoheader
	$ ls
	autom4te.cache  autoscan.log  config.h.in  configure.ac  hw.c

At the moment there's no need to change anything in `config.h.in`. It's possible
to run `autoconf` at this stage which will generate the configure script:

	$ autoconf
	$ ls
	autom4te.cache  autoscan.log  config.h.in  configure  configure.ac  hw.c
	$ ./configure
	checking for gcc... gcc
	checking whether the C compiler works... yes
	checking for C compiler default output file name... a.out
	checking for suffix of executables...
	checking whether we are cross compiling... no
	checking for suffix of object files... o
	checking whether we are using the GNU C compiler... yes
	checking whether gcc accepts -g... yes
	checking for gcc option to accept ISO C89... none needed
	configure: creating ./config.status
	config.status: creating config.h

The configure doesn't do much besides generating the `config.h` file which we can
use in our `hw.c` program. It contains some basic information generated from
`configure.ac` file:

```
/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "hw@hw.com"

/* Define to the full name of this package. */
#define PACKAGE_NAME "hello_world"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "hello_world 0.1"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "hello_world"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "0.1"
```

Let's display the version information in our program as well, using the
information from config.h:

```
#include <stdio.h>
#include "config.h"

int main(int argc, char *argv[])
{
        printf("Hello, World\n");
        printf("Version: %s\n", PACKAGE_VERSION);
        return 0;
}
```

It's time to involve `automake` in the process now.


## automake

Automake reads it's configuration from Makefile.am files. We should generate
Makefile.am file for every directory containing source files and place
references in the root Makefile.am to every each of them. Since, the example
project is trivial I need only one `Makefile.am`. It should contain the
following data:

    $ cat Makefile.am

    bin_PROGRAMS=hw

    hw_SOURCES=hw.c


It's pretty straight forward. The first line lists the executable which should
be created. The second line lists the source files for a particular executable
(in this case hw - hello world).


We can now run `automake`:

```
$ automake --add-missing
configure.ac: error: no proper invocation of AM_INIT_AUTOMAKE was found.
configure.ac: You should verify that configure.ac invokes AM_INIT_AUTOMAKE,
configure.ac: that aclocal.m4 is present in the top-level directory,
configure.ac: and that aclocal.m4 was recently regenerated (using aclocal)
automake: error: no 'Makefile.am' found for any configure output
automake: Did you forget AC_CONFIG_FILES([Makefile]) in configure.ac?
```

... which tells us exactly what must be done in configure.ac to make `automake`
cooperate. Let's add `AM_INIT_AUTOMAKE` and `AC_CONFIG_FILES([Makefile])` to
configure.ac:

```
#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_PREREQ([2.69])
AC_INIT([hello_world], [0.1], [hw@hw.com])
AC_CONFIG_SRCDIR([.])
AC_CONFIG_HEADERS([config.h])

AM_INIT_AUTOMAKE
AC_CONFIG_FILES([Makefile])

# Checks for programs.
AC_PROG_CC

# Checks for libraries.

# Checks for header files.

# Checks for typedefs, structures, and compiler characteristics.

# Checks for library functions.

AC_OUTPUT
```

Let's do what automake asks us to do as well:

	$ aclocal
	$ autoconf

...and perform another attempt of automake:


```
$ automake --add-missing
configure.ac:13: installing './compile'
configure.ac:9: installing './install-sh'
configure.ac:9: installing './missing'
Makefile.am: error: required file './NEWS' not found
Makefile.am: error: required file './README' not found
Makefile.am: error: required file './AUTHORS' not found
Makefile.am: error: required file './ChangeLog' not found
```

That looks much better. Let's add the missing files to the project and re-run:

	$ touch ./NEWS ./README ./AUTHORS ./ChangeLog
	$ automake --add-missing

The directory now should like so:

```
AUTHORS    INSTALL      NEWS        autom4te.cache  config.h.in   depcomp     missing
COPYING    Makefile.am  README      autoscan.log    configure     hw.c
ChangeLog  Makefile.in  aclocal.m4  compile         configure.ac  install-sh
```

The project is more or less complete. The configure script should now generate
a Makefile and it should be possible to build the project:

```
$ ./configure
checking for a BSD-compatible install... /usr/bin/install -c
checking whether build environment is sane... yes
checking for a thread-safe mkdir -p... /usr/bin/mkdir -p
checking for gawk... gawk
checking whether make sets $(MAKE)... yes
checking whether make supports nested variables... yes
checking for gcc... gcc
checking whether the C compiler works... yes
checking for C compiler default output file name... a.out
checking for suffix of executables...
checking whether we are cross compiling... no
checking for suffix of object files... o
checking whether we are using the GNU C compiler... yes
checking whether gcc accepts -g... yes
checking for gcc option to accept ISO C89... none needed
checking whether gcc understands -c and -o together... yes
checking for style of include used by make... GNU
checking dependency style of gcc... gcc3
checking that generated files are newer than configure... done
configure: creating ./config.status
config.status: creating Makefile
config.status: creating config.h
config.status: executing depfiles commands
```

```
tomasz@iris hw [0] (master)$ ls
AUTHORS    Makefile     README          compile      config.status  hw.c
COPYING    Makefile.am  aclocal.m4      config.h     configure      install-sh
ChangeLog  Makefile.in  autom4te.cache  config.h.in  configure.ac   missing
INSTALL    NEWS         autoscan.log    config.log   depcomp        stamp-h1
```


```
tomasz@iris hw [0] (master)$ make
(CDPATH="${ZSH_VERSION+.}:" && cd . && /bin/sh /home/tomasz/grive/hw/missing autoheader)
rm -f stamp-h1
touch config.h.in
cd . && /bin/sh ./config.status config.h
config.status: creating config.h
make  all-am
make[1]: Entering directory '/home/tomasz/grive/hw'
gcc -DHAVE_CONFIG_H -I.     -g -O2 -MT hw.o -MD -MP -MF .deps/hw.Tpo -c -o hw.o hw.c
mv -f .deps/hw.Tpo .deps/hw.Po
gcc  -g -O2   -o hw hw.o
make[1]: Leaving directory '/home/tomasz/grive/hw'
```

```
tomasz@iris hw [0] (master)$ ls
AUTHORS    Makefile     README          compile       config.log     depcomp  install-sh
COPYING    Makefile.am  aclocal.m4      config.h      config.status  hw       missing
ChangeLog  Makefile.in  autom4te.cache  config.h.in   configure      hw.c     stamp-h1
INSTALL    NEWS         autoscan.log    config.h.in~  configure.ac   hw.o
```

```
tomasz@iris hw [0] (master)$ ./hw
Hello, World
Version: 0.1
```

During the development a small script to recreate the whole environment from
scratch might become handy:

```
$ touch autogen.sh
$ chmod +x autogen,sh
cat > autogen.sh
aclocal
autoconf
automake
EOF
```
Other option is to use a call to `autoreconf -a` which is equivalent to all the
operations above.

It's possible now to create a software package for our program:

```
$ make distcheck
make  dist-gzip am__post_remove_distdir='@:'
make[1]: Entering directory '/home/tomasz/grive/hw'
if test -d "hello_world-0.1"; then find "hello_world-0.1" -type d ! -perm -200 -exec chmod u+w {} ';' && rm -rf "hello_world-0.1" || { sleep 5 && rm -rf "hello_world-0.1"; }; else :; fi
test -d "hello_world-0.1" || mkdir "hello_world-0.1"
test -n "" \
|| find "hello_world-0.1" -type d ! -perm -755 \
        -exec chmod u+rwx,go+rx {} \; -o \
  ! -type d ! -perm -444 -links 1 -exec chmod a+r {} \; -o \
  ! -type d ! -perm -400 -exec chmod a+r {} \; -o \
  ! -type d ! -perm -444 -exec /bin/sh /home/tomasz/grive/hw/install-sh -c -m a+r {} {} \; \
|| chmod -R a+r "hello_world-0.1"
tardir=hello_world-0.1 && ${TAR-tar} chof - "$tardir" | GZIP=--best gzip -c >hello_world-0.1.tar.gz
make[1]: Leaving directory '/home/tomasz/grive/hw'
if test -d "hello_world-0.1"; then find "hello_world-0.1" -type d ! -perm -200 -exec chmod u+w {} ';' && rm -rf "hello_world-0.1" || { sleep 5 && rm -rf "hello_world-0.1"; }; else :; fi
case 'hello_world-0.1.tar.gz' in \
*.tar.gz*) \
  GZIP=--best gzip -dc hello_world-0.1.tar.gz | ${TAR-tar} xf - ;;\
*.tar.bz2*) \
  bzip2 -dc hello_world-0.1.tar.bz2 | ${TAR-tar} xf - ;;\
*.tar.lz*) \
  lzip -dc hello_world-0.1.tar.lz | ${TAR-tar} xf - ;;\
*.tar.xz*) \
  xz -dc hello_world-0.1.tar.xz | ${TAR-tar} xf - ;;\
*.tar.Z*) \
  uncompress -c hello_world-0.1.tar.Z | ${TAR-tar} xf - ;;\
*.shar.gz*) \
  GZIP=--best gzip -dc hello_world-0.1.shar.gz | unshar ;;\
*.zip*) \
  unzip hello_world-0.1.zip ;;\
esac
chmod -R a-w hello_world-0.1
chmod u+w hello_world-0.1
mkdir hello_world-0.1/_build hello_world-0.1/_inst
chmod a-w hello_world-0.1
test -d hello_world-0.1/_build || exit 0; \
dc_install_base=`CDPATH="${ZSH_VERSION+.}:" && cd hello_world-0.1/_inst && pwd | sed -e 's,^[^:\\/]:[\\/],/,'` \
  && dc_destdir="${TMPDIR-/tmp}/am-dc-$$/" \
  && am__cwd=`pwd` \
  && CDPATH="${ZSH_VERSION+.}:" && cd hello_world-0.1/_build \
  && ../configure \
     \
     \
    --srcdir=.. --prefix="$dc_install_base" \
  && make  \
  && make  dvi \
  && make  check \
  && make  install \
  && make  installcheck \
  && make  uninstall \
  && make  distuninstallcheck_dir="$dc_install_base" \
        distuninstallcheck \
  && chmod -R a-w "$dc_install_base" \
  && ({ \
       (cd ../.. && umask 077 && mkdir "$dc_destdir") \
       && make  DESTDIR="$dc_destdir" install \
       && make  DESTDIR="$dc_destdir" uninstall \
       && make  DESTDIR="$dc_destdir" \
            distuninstallcheck_dir="$dc_destdir" distuninstallcheck; \
      } || { rm -rf "$dc_destdir"; exit 1; }) \
  && rm -rf "$dc_destdir" \
  && make  dist \
  && rm -rf hello_world-0.1.tar.gz \
  && make  distcleancheck \
  && cd "$am__cwd" \
  || exit 1
checking for a BSD-compatible install... /usr/bin/install -c
checking whether build environment is sane... yes
checking for a thread-safe mkdir -p... /usr/bin/mkdir -p
checking for gawk... gawk
checking whether make sets $(MAKE)... yes
checking whether make supports nested variables... yes
checking for gcc... gcc
checking whether the C compiler works... yes
checking for C compiler default output file name... a.out
checking for suffix of executables...
checking whether we are cross compiling... no
checking for suffix of object files... o
checking whether we are using the GNU C compiler... yes
checking whether gcc accepts -g... yes
checking for gcc option to accept ISO C89... none needed
checking whether gcc understands -c and -o together... yes
checking for style of include used by make... GNU
checking dependency style of gcc... gcc3
checking that generated files are newer than configure... done
configure: creating ./config.status
config.status: creating Makefile
config.status: creating config.h
config.status: executing depfiles commands
make[1]: Entering directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
make  all-am
make[2]: Entering directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
gcc -DHAVE_CONFIG_H -I. -I..     -g -O2 -MT hw.o -MD -MP -MF .deps/hw.Tpo -c -o hw.o ../hw.c
mv -f .deps/hw.Tpo .deps/hw.Po
gcc  -g -O2   -o hw hw.o
make[2]: Leaving directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
make[1]: Leaving directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
make[1]: Entering directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
make[1]: Nothing to be done for 'dvi'.
make[1]: Leaving directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
make[1]: Entering directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
make[1]: Nothing to be done for 'check'.
make[1]: Leaving directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
make[1]: Entering directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
make[2]: Entering directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
 /usr/bin/mkdir -p '/home/tomasz/grive/hw/hello_world-0.1/_inst/bin'
  /usr/bin/install -c hw '/home/tomasz/grive/hw/hello_world-0.1/_inst/bin'
make[2]: Nothing to be done for 'install-data-am'.
make[2]: Leaving directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
make[1]: Leaving directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
make[1]: Entering directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
make[1]: Nothing to be done for 'installcheck'.
make[1]: Leaving directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
make[1]: Entering directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
 ( cd '/home/tomasz/grive/hw/hello_world-0.1/_inst/bin' && rm -f hw )
make[1]: Leaving directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
make[1]: Entering directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
make[1]: Leaving directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
make[1]: Entering directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
make[2]: Entering directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
 /usr/bin/mkdir -p '/tmp/am-dc-6606//home/tomasz/grive/hw/hello_world-0.1/_inst/bin'
  /usr/bin/install -c hw '/tmp/am-dc-6606//home/tomasz/grive/hw/hello_world-0.1/_inst/bin'
make[2]: Nothing to be done for 'install-data-am'.
make[2]: Leaving directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
make[1]: Leaving directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
make[1]: Entering directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
 ( cd '/tmp/am-dc-6606//home/tomasz/grive/hw/hello_world-0.1/_inst/bin' && rm -f hw )
make[1]: Leaving directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
make[1]: Entering directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
make[1]: Leaving directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
make[1]: Entering directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
make  dist-gzip am__post_remove_distdir='@:'
make[2]: Entering directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
if test -d "hello_world-0.1"; then find "hello_world-0.1" -type d ! -perm -200 -exec chmod u+w {} ';' && rm -rf "hello_world-0.1" || { sleep 5 && rm -rf "hello_world-0.1"; }; else :; fi
test -d "hello_world-0.1" || mkdir "hello_world-0.1"
test -n "" \
|| find "hello_world-0.1" -type d ! -perm -755 \
        -exec chmod u+rwx,go+rx {} \; -o \
  ! -type d ! -perm -444 -links 1 -exec chmod a+r {} \; -o \
  ! -type d ! -perm -400 -exec chmod a+r {} \; -o \
  ! -type d ! -perm -444 -exec /bin/sh /home/tomasz/grive/hw/hello_world-0.1/install-sh -c -m a+r {} {} \; \
|| chmod -R a+r "hello_world-0.1"
tardir=hello_world-0.1 && ${TAR-tar} chof - "$tardir" | GZIP=--best gzip -c >hello_world-0.1.tar.gz
make[2]: Leaving directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
if test -d "hello_world-0.1"; then find "hello_world-0.1" -type d ! -perm -200 -exec chmod u+w {} ';' && rm -rf "hello_world-0.1" || { sleep 5 && rm -rf "hello_world-0.1"; }; else :; fi
make[1]: Leaving directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
make[1]: Entering directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
test -z "hw" || rm -f hw
rm -f *.o
rm -f *.tab.c
test -z "" || rm -f
test . = ".." || test -z "" || rm -f
rm -f config.h stamp-h1
rm -f TAGS ID GTAGS GRTAGS GSYMS GPATH tags
rm -f cscope.out cscope.in.out cscope.po.out cscope.files
rm -f config.status config.cache config.log configure.lineno config.status.lineno
rm -rf ./.deps
rm -f Makefile
make[1]: Leaving directory '/home/tomasz/grive/hw/hello_world-0.1/_build'
if test -d "hello_world-0.1"; then find "hello_world-0.1" -type d ! -perm -200 -exec chmod u+w {} ';' && rm -rf "hello_world-0.1" || { sleep 5 && rm -rf "hello_world-0.1"; }; else :; fi
=================================================
hello_world-0.1 archives ready for distribution:
hello_world-0.1.tar.gz
=================================================
```

The `hello_world-0.1.tar.gz` package is ready to be redistributed.

Further reading & references:
[autoconf & automake](http://mij.oltrelinux.com/devel/autoconf-automake/)
[autotools](https://developer.gnome.org/anjuta-build-tutorial/stable/create-autotools.html.en)
[autotools](http://markuskimius.wikidot.com/programming:tut:autotools:2)
