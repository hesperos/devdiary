---
title: "Permutations"
date: 2021-01-02T14:33:01Z
draft: false
tags: ['permutations', 'golang', 'narayanapandita', 'heap']
author: Tomasz Wisniewski (hesperos)
summary: practical ways to generate permutations of a set
---

This repo contains a summary of most practically useful permutation generation
algorithms: [permutations](https://gitlab.com/hesperos/permutations).


References:
1. [Fisher Yates Shuffle Wiki](https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle)
2. [Permutations Wiki](https://en.wikipedia.org/wiki/Permutation#Generation_in_lexicographic_order)
3. [Steinhaus Johnson Trotter Wiki](https://en.wikipedia.org/wiki/Steinhaus%E2%80%93Johnson%E2%80%93Trotter_algorithm)
4. [Heap's Algorithm Wiki](https://en.wikipedia.org/wiki/Heap%27s_algorithm)
5. [project nayuki](https://www.nayuki.io/page/next-lexicographical-permutation-algorithm)
6. [Permutations, Rosetta Code](https://rosettacode.org/wiki/Permutations)
