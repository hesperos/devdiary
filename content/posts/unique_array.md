---
title: "Find a unique set of numbers in an array"
date: 2021-02-26T13:54:24Z
draft: false
tags: [ 'golang', 'algorithms', 'unique' ]
author: Tomasz Wisniewski (hesperos)
summary: A simple algorithm running in O(n), grouping unique numbers in the array in its very beginning.
---

Can't find the source where I originally found this algorithm. Nevertheless
it's a pretty clever short algorithm that groups all unique numbers in a given
array in its initial part. Since it compares last unique element with the
remainder of the array, obviously it requires sorted data.

Below is a quick implementation in golang:

```
func Unique(data []int) []int {
	b := 0
	for a := 1; a < len(data); a++ {
		if data[a] != data[b] {
			b++
			data[b] = data[a]
		}
	}

	if len(data) > 0 {
		return data[:(b + 1)]
	}

	// empty slice?
	return data
}
```

Repository: https://gitlab.com/hesperos/unique
