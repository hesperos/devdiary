---
title: "C++ functional composition"
date: 2017-06-10T16:19:27+01:00
draft: false
tags: [ "c++" ]
author: Tomasz Wisniewski (hesperos)
summary: This posts describes a simple approach to functional composition. Functionality is split across small independent functors which are independently composable.
---

This is a small trick allowing to compose a set of functions together. Assuming
that we have a set of functions `f`, `g`, `h`, one of the function compositions may
be: `f(g(h(arguments)))`. This can be done manually of course but then we are
loosing flexibility and it's impossible to compose more complex functors
together. Variadic templates can be used to achieve this scenario.


```
#include <iostream>
#include <boost/lexical_cast.hpp>

template <typename DestT>
struct LexicalCast
{
    template <typename SrcT>
    DestT operator()(SrcT&& value, DestT defaultValue = DestT{}) const
    {
        try
        {
            return boost::lexical_cast<DestT>(std::forward<SrcT>(value));
        }
        catch(const boost::bad_lexical_cast&)
        {
        }

        return defaultValue;
    }
};

template <typename ArgT, ArgT N>
struct Double
{
    constexpr ArgT operator()(ArgT arg) const
    {
        return N * arg;
    };
};

template <typename FunctorT, typename ... FunctorsT>
struct Compose
{
    template <typename ... ArgsT>
    typename std::result_of<
        FunctorT(typename std::result_of<Compose<FunctorsT...>(ArgsT...)>::type)
        >::type operator()(ArgsT&&... args)
    {
        return FunctorT{}(Compose<FunctorsT...>{}(
                    std::forward<ArgsT>(args)...));
    }
};

template <typename FunctorT>
struct Compose<FunctorT>
{
    template <typename ... ArgsT>
    typename std::result_of<FunctorT(ArgsT...)>::type
        operator()(ArgsT&&... args)
    {
        return FunctorT{}(std::forward<ArgsT>(args)...);
    }
};

template <typename NumericT, NumericT N>
using StringMultiplier =
Compose<Double<NumericT, N>, LexicalCast<NumericT>>;

int main(int argc, const char *argv[])
{
    std::cout << StringMultiplier<int, 3>{}("10") << std::endl;
    return 0;
}
```
