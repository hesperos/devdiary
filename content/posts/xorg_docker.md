---
title: "Running Xorg apps in docker container"
date: 2017-08-25T15:16:54+01:00
draft: false
tags: [ "docker", "xorg" ]
author: Tomasz Wisniewski (hesperos)
summary: This is a quick reference/reminder on how to launch x11 apps from within docker containers.
---

Example Dockerfile:

```
FROM ubuntu

RUN apt-get update -y
RUN apt-get install -y x11-apps

ENTRYPOINT [ "xeyes"  ]
```

Building the image and starting the container:

    docker build -it xtest .

    docker run \
        -u $(id -u):$(id -g) \
        -v $HOME/.Xauthority:/home/developer/.Xauthority \
        -v /tmp/.X11-unix:/tmp/.X11-unix \
        -e DISPLAY=$DISPLAY \
        --net=host \
        -it xtest
