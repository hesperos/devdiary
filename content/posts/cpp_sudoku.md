---
title: "Sudoku solver in c++"
date: 2015-01-17T16:40:50+01:00
draft: false
tags: ["c++", "sudoku"]
author: Tomasz Wisniewski (hesperos)
summary: This posts describes a simple recursive, brute-force sudoku solver implementation.
---

Just for fun (and to practice C++ of course) I wrote a simple sudoku solving
program. Mainly I wanted to practice the use of shared pointers and basic
object oriented design. I come up with the following class diagram:

![Class diagram of the sudoku program](/devdiary/img/sudoku_uml.png)

That's a relatively simple program, but anyway there are some classes contained.
First of all I wanted to visualize the backtracking method of solving the
sudoku board. In great shortcut, the machine tries out all the combinations and
with every try it checks if the sudoku constraints aren't violated (no
repeating numbers in the column, row and a box) if not, it will
continue with another field, if they are it'll backtrack and try with another
value. This process continues until the whole board is processed.

It's not the fastest method, but that wasn't really my goal. Coming back to the
class diagram. I implemented a classic observer pattern. There are two classes
which act as an interface both for `observees` and `observers`. The `board` class
inherits from the `observee`, the `printer` class inherits from the `observer`.

![Visualisation of the backtracking method](/devdiary/img/optimized.gif)

The `printer` object registers itself as an `observer` of the `board`. Whenever any
change is being done to the `board`, the `board` object updates all of it's
observers. Whenever the `printer` object receives an update notification it
simply reads the data of it's `observee` and prints the whole board to the
standard output. The `observer` pattern is very useful here and solves a lot of
problems. That's the core of the application. Besides that there are two more
interfaces and their implementations.


The `board_reader` is an interface which defines a way to read the board. The
solver defines an interface to solve the sudoku board.



The `csv_reader` is a `board_reader` implementation. It reads a text file which
should contain 9 lines each containing 9 comma separated digits dedicated for
each field of the sudoku board. The zero means an empty field.



The `bt_solver` implements a back tracking method of solving the game. 



This design is very flexible. It's easy to implement any additional method to
read the board. It's very easy to implement any new solving methods. Even
changing the output (the printer) is fairly easy. The whole thing is a set of a
building blocks. It's even possible to support boards bigger or smaller than
the classic 9x9 one, since all the classes are templates of the board size.



How to use this code ? Let's look on the main function and discuss it line by
line.

```
std::string bf = (argc >=2) ? argv[1] : "board1.csv";
bool intermediate = false;
int delay = 0;

if (argc==1) {
    std::cout << "./sudoku <board_file> [intermediate=0|1] [delay=i]" << std::endl;
    return -1;
}

if (argc >= 3) {
    std::istringstream(std::string(argv[2])) >> intermediate;
}

if (argc >=4) {
    std::istringstream(std::string(argv[3])) >> delay;
}

// board
auto b = std::make_shared< board<3> >();

// board reader
auto br = std::make_shared< csv_reader<3> >(b);
br->load(bf);

// solver
auto s = std::make_shared< bt_solver<3> >(b, delay);

// printer
auto p = std::make_shared< printer<3> >(b);

// initialize the printer
p->init();

// detach observer & print only the result
if (!intermediate) b->del(p);

// solve the puzzle
s->solve();

// print the result
if (!intermediate) p->print();
```


First of all the argument line provides two options (besides the file name
containing the unsolved board definition). The first one either 0 or 1 - to
print or omit the intermediate results. What this option really does it simply
disconnects the `observer` (`printer`) from the `observee` (`board`) when it's value is
zero - meaning we don't want the intermediate results. That's another benefit
of having the observer pattern. The second argument is the delay time between
the steps. By default the solver will go as fast as possible.


Once we have the arguments, the objects are instantiated (starting from line
34). An instance of board is created. Template value 3 means we want the 3x3
boxes, value of 4 would create 16 4x4 boxes. Once we've got the board a
`board_reader` is created for that board instance and called with the provided
file name. Once that's done we have a board populated with data ready to be
solved. An instance of solver is created for our board and an instance of
printer.



Later on, I detach the printer if we're not interested in intermediate results.
Finally we solve the board. Since in case no intermediates we need to print the
final result manually since the observer is detached.



Summarizing, this was a pretty pleasant exercise. The full code can be obtained
from [gitlab](https://gitlab.com/hesperos/dev_Sudoku):
