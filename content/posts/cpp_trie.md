---
title: "Implementing simple trie prefix tree"
date: 2019-08-12T16:31:57+01:00
draft: false
tags: ["c++", "trie"]
author: Tomasz Wisniewski (hesperos)
summary: This is a simple proof of concent trie implementation - to understand better the mechanics of this data structure.
---

This is a trivial proof of concept implementation of trie, as described on [wiki](https://en.wikipedia.org/wiki/Trie).

![trie](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5d/Pointer_implementation_of_a_trie.svg/1200px-Pointer_implementation_of_a_trie.svg.png)

```
#include <algorithm>
#include <iterator>
#include <memory>
#include <string>
#include <vector>
#include <fstream>

#include <iostream>

namespace {

    class Trie {
    public:
        void insert(const std::string& entry);
        std::vector<std::string> find(const std::string& prefix) const;
        size_t count() const;
        size_t memory() const;

    private:
        struct Node {
            Node(const char character) :
                character(character)
            {
                nodes++;
            }

            ~Node() {
                nodes--;
            }

            const char character;
            std::unique_ptr<Node> next;
            std::unique_ptr<Node> children;
            static size_t nodes;
        };

        std::unique_ptr<Node> root;

        void insertCharacter(std::unique_ptr<Node>& node,
                const std::string& entry,
                size_t position);

        void findPrefix(const std::string& prefix,
                const std::unique_ptr<Node>& node,
                std::vector<std::string>& results,
                std::string tmp) const;
    };

    size_t Trie::Node::nodes = 0;

    void Trie::insert(const std::string& entry) {
        insertCharacter(root, entry, 0);
    }

    std::vector<std::string> Trie::find(const std::string& prefix) const {
        std::vector<std::string> results;
        std::string tmp;
        findPrefix(prefix, root, results, tmp);
        return results;
    }

    size_t Trie::count() const {
        return Node::nodes;
    }

    size_t Trie::memory() const {
        return Node::nodes * sizeof(Trie::Node);
    }

    void Trie::insertCharacter(std::unique_ptr<Node>& node,
            const std::string& entry,
            size_t position) {
        if (position >= entry.length()) {
            return;
        }

        if (!node) {
            node = std::make_unique<Node>(entry.at(position));
            insertCharacter(node->children, entry, ++position);
        }
        else if (node->character == entry.at(position)) {
            insertCharacter(node->children, entry, ++position);
        }
        else {
            insertCharacter(node->next, entry, position);
        }
    }

    void Trie::findPrefix(const std::string& prefix,
            const std::unique_ptr<Node>& node,
            std::vector<std::string>& results,
            std::string tmp) const
    {
        if (!node) {
            return;
        }

        if (tmp.length() < prefix.length()) {
            if (prefix.at(tmp.length()) == node->character) {
                tmp.append(1, node->character);
                findPrefix(prefix, node->children, results, tmp);
            }
            else {
                findPrefix(prefix, node->next, results, tmp);
            }
        }
        else {
            if (!node->children) {
                tmp.append(1, node->character);
                results.push_back(tmp);
            }
            else {
                tmp.append(1, node->character);
                findPrefix(prefix, node->children, results, tmp);
            }

            if (node->next) {
                tmp.erase(tmp.length() - 1);
                findPrefix(prefix, node->next, results, tmp);
            }
        }
    }
}

int main(int argc, const char *argv[])
{
    Trie trie;
    std::ifstream ifs("/usr/share/dict/british-english");
    std::string word;
    bool isRunning = true;

    std::cout << "Reading dictionary...";
    while (ifs >> word) {
        trie.insert(word);
    }
    std::cout << "done" << std::endl;
    std::cout << "Total nodes: " << trie.count() << ", memory: " << trie.memory() << "\n";

    auto printer = [&](const std::string& prefix) {
        const auto& results = trie.find(prefix);
        std::cout << "Results for prefix: [" << prefix << "]\n";
        std::copy(results.begin(),
                results.end(),
                std::ostream_iterator<std::string>(std::cout, "\n"));
        std::cout << "====" << std::endl;
    };

    std::cout << "Type prefix to search, or exit to abort" << std::endl;
    while (isRunning) {
        std::string prefix;
        std::cin >> prefix;

        if (prefix == "exit") {
            isRunning = false;
        }

        printer(prefix);
    }

    return 0;
}
```



