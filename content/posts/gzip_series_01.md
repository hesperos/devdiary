---
title: "Writing gzip decompressor: Reading the header"
date: 2019-07-09T12:57:26+01:00
draft: false
tags: [ 'gzip', 'golang', 'huffman' ]
author: Tomasz Wisniewski (hesperos)
summary: This is a part of a series where I describe my attempts to implement a gzip decompressor from scratch in golang. This part focuses on the general file structure and gzip header.
---

# Why bother?

Huffman coding and Huffman trees are a fundamental CS topic that everyone
studies about sooner or later. Regardless if you have any CS academic
background or not, eventually you'll bump into that topic. Playing around with
some custom file formats and toy compressors, decompressors is fun but why not
try to understand how this technology is used in a production ready
applications? gzip is everywhere it's a common compression method used
abundantly in Unix world, it's used in HTTP compression. It's a well adopted
standard with long traditions. Learning how it works for sure brings a lot of
details on how to design an efficient (both in terms of performance and
compression-wise) compression method employing Huffman coding. Reading RFC is
one thing but, in my opinion, attempting to implement something from the very
beginning helps to understand better everything that's really happening.

In this series I'll try to implement a working gzip decompressor in _golang_. Why
_golang_? No real reasons, it can be really any language of choice, however,
having the Reader/Writer interfaces in _golang_ makes it especially
appealing for this purpose and this is a language I don't have an occasion to
use on a daily basis so, it's a great opportunity to practice.

# Reference

gzip is everywhere. On any `*nix`-like OS, any of these implementations can
serve as a reference for troubleshooting. RFCs are probably the most obvious
first hand piece of information about the details:

* [GZIP file format specification](https://www.ietf.org/rfc/rfc1952.txt)
* [DEFLATE Compressed Data Format Specification](https://www.ietf.org/rfc/rfc1951.txt)

Implementations:

* [zlib](https://github.com/madler/zlib)
* [zlib docs](https://github.com/madler/zlib/raw/master/doc/algorithm.txt)

Implementation for your language of choice is a good reference as well. In case
of _golang_ that is, of course, `compress/gzip` and `inflate.go` - as a reference
for decompression algorithm.

# GZIP file format

First, let's understand the file structure and implement some structure to hold
the details about the input file. First, some project skeleton is required and
some testdata.

    $ mkdir -p testdata
    $ cd testdata
    $ wget https://www.ietf.org/rfc/rfc1952.txt
    $ gzip -k rfc1952.txt

Let's create the decompressor package and its test. This is where I'm going to
attempts to implement the `inflate` algorithm:

    $ touch gzd{,_test}.go

So far, our project should have the following structure:

```
.:
gzd.go  gzd_test.go  testdata

./testdata:
rfc1952.txt rfc1952.txt.gz
```

# Project skeleton

Let's implement the first trivial test to bootstrap the project:

Here's the initial contents of `gzd_test.go`:

```
package gogz

import (
    "os"
    "testing"
)

func TestIfItsPossibleToCreateAReader(t *testing.T) {
    inputFh, err := os.Open("testdata/rfc1952.txt.gz")
    if err != nil {
        t.Fatalf("unable to open test asset: %s\n", err)
    }
    defer inputFh.Close()

    reader, err := NewReader(inputFh)
    if err != nil {
        t.Fatalf("error when creating gogz Reader: %s\n", err)
    }

    defer reader.Close()
}
```

... and the initial implementation of the decompressor:

```
package gogz

import (
    "io"
)

type GoGzReader struct {
    reader io.Reader
}

func (r *GoGzReader) Read(p []byte) (int, error) {
    return 0, nil
}

func (r *GoGzReader) Close() error {
    return nil
}

func NewReader(r io.Reader) (*GoGzReader, error) {
    reader := GoGzReader{
        reader: r,
    }
    return &reader, nil
}
```

So far, so good. Not much is happening but we defined the `GoGzReader` type and
some initial boiler-plate code to work with. Our trivial test is passing. The
`GoGzReader` implements `io.Reader` and `io.Closer` interfaces.

# Reading the _data member's_ header

Let's get some data from the file. As desribed in *rfc1952.txt*, _gzip_ file is
comprised of a series of _data members_. Each _data member_ has its preceding
header. It's up to the compressor to split the content into multiple _data
members_. Each _data member_ header starts with a magic sequence id1, id2
(0x1f, 0x8b), followed by information regarding the system that produced the
file, the file name (if available), comment, compression method used (deflate -
most often) etc. For each _data member_, following the compressed data,
there's an 8 byte footer containing the crc32 for the input data and its
length in bytes. Most of the time a single file contains one _data member_
only. Let's model the _data member's_ header and footer and add some code
to read it. First some constants and data types:


```
type Os uint8

type CompressionMethod uint8

type Magic uint8

const (
    Id1 Magic = 0x1f
    Id2 Magic = 0x8b
)

const (
    CmDeflate CompressionMethod = 8
)

const (
    OsFat Os = iota
    OsAmiga
    OsVms
    OsUnix
    OsVmCms
    OsAtariTos
    OsHpfs
    OsMacintosh
    OsZSystem
    OsCpm
    OsTops20
    OsNtfs
    OsQDos
    OsRiscos
    OsUnknown
)

type Flags struct {
    Flags uint8
}

func (f Flags) IsText() bool {
    return (f.Flags & (1 << 0x00) != 0x00)
}

func (f Flags) HasCrc16() bool {
    return (f.Flags & (1 << 0x01) != 0x00)
}

func (f Flags) HasExtraFields() bool {
    return (f.Flags & (1 << 0x02) != 0x00)
}

func (f Flags) HasFileName() bool {
    return (f.Flags & (1 << 0x03) != 0x00)
}

func (f Flags) HasComment() bool {
    return (f.Flags & (1 << 0x04) != 0x00)
}

type MemberHeader struct {
    CompressionMethod CompressionMethod
    Flg Flags
    ModificationTime time.Time
    Xfl uint8
    Os Os
    FileName string
    Comment string
    HeaderCrc16 uint16
}

type MemberFooter struct {
    Crc32 uint32
    InputSize uint32
}

type Member struct {
    Header *MemberHeader
    Footer *MemberFooter
}

```

With all of that we're ready to read the header.


```
func isGzMemberHeader(r io.Reader) (bool, error) {
    var ids [2]uint8
    n, err := io.ReadFull(r, ids[:])

    if err != nil {
        return false, err
    }
    if n != len(ids) {
        return false, fmt.Errorf("unable to read gz header\n")
    }
    if Magic(ids[0]) != Id1 || Magic(ids[1]) != Id2 {
        return false, fmt.Errorf("invalid gz header found\n")
    }

    return true, nil
}

func NewMemberHeader(r io.Reader) (*MemberHeader, error) {
    var mtime uint32 = 0
    header := MemberHeader{}

    // created buffered reader
    br := bufio.NewReader(r)

    if ok, err := isGzMemberHeader(br); !ok {
        return nil, err
    }

    if err := binary.Read(br, binary.LittleEndian, &header.CompressionMethod); err != nil {
        return nil, fmt.Errorf("unable to get the compression-method: %v\n", err)
    }

    if err := binary.Read(br, binary.LittleEndian, &header.Flg.Flags); err != nil {
        return nil, fmt.Errorf("unable to get the member flags: %v\n", err)
    }

    if err := binary.Read(br, binary.LittleEndian, &mtime); err != nil {
        return nil, fmt.Errorf("unable to get the modification-time: %v\n", err)
    }
    header.ModificationTime = time.Unix(int64(mtime), 0)

    if err := binary.Read(br, binary.LittleEndian, &header.Xfl); err != nil {
        return nil, fmt.Errorf("unable to get the xfl: %v\n", err)
    }

    if err := binary.Read(br, binary.LittleEndian, &header.Os); err != nil {
        return nil, fmt.Errorf("unable to get the os: %v\n", err)
    }

    if header.Flg.HasExtraFields() {
        // this is not supported, just skip this part if it's present
        var xlen uint16 = 0
        if err := binary.Read(br, binary.LittleEndian, &xlen); err != nil {
            return nil, fmt.Errorf("unable to get the xlen: %v\n", err)
        }
        br.Discard(int(xlen))
    }

    if header.Flg.HasFileName() {
        fname, err := br.ReadString(0)
        if err != nil {
            return nil, fmt.Errorf("unable to get the filename: %v\n", err)
        }
        header.FileName = strings.TrimRight(fname, "\x00")
    }

    if header.Flg.HasComment() {
        comment, err := br.ReadString(0)
        if err != nil {
            return nil, fmt.Errorf("unable to get the comment: %v\n", err)
        }
        header.Comment = strings.TrimRight(comment, "\x00")
    }

    if header.Flg.HasCrc16() {
        if err := binary.Read(br, binary.LittleEndian, header.HeaderCrc16); err != nil {
            return nil, fmt.Errorf("unable to get the xlen: %v\n", err)
        }
    }

    return &header, nil
}

func NewMemberFooter(r io.Reader) (*MemberFooter, error) {
    footer := MemberFooter{}

    if err := binary.Read(r, binary.LittleEndian, footer.Crc32); err != nil {
        return nil, fmt.Errorf("unable to get the crc32: %v\n", err)
    }

    if err := binary.Read(r, binary.LittleEndian, footer.InputSize); err != nil {
        return nil, fmt.Errorf("unable to get the input-size: %v\n", err)
    }

    return &footer, nil
}

func NewMember(r io.Reader) (*Member, error) {
    header, err := NewMemberHeader(r)
    if err != nil {
        return nil, err
    }

    member := Member{
        Header: header,
    }
    return &member, nil
}
```


When creating a new GoGzReader it would be good to obtain details about first
_data member_ (if any) and report an error if the file doesn't seem to be a gzip
file. For the moment lets do that in the constructor function, which ultimately
looks like so:


```
func NewReader(r io.Reader) (*GoGzReader, error) {
    member, err := NewMember(r)
    if err != nil {
        return nil, err
    }

    reader := GoGzReader{
        reader: r,
        currentMember: member,
    }

    return &reader, nil
}
```

This allows to create another unit test verfying the contents of the read
_data member_ header from a known file.

```
func TestIfItsPossibleToObtainFileHeader(t *testing.T) {
    inputFh, err := os.Open("testdata/rfc1952.txt.gz")
    if err != nil {
        t.Fatalf("unable to open test asset: %s\n", err)
    }
    defer inputFh.Close()

    reader, err := NewReader(inputFh)
    if err != nil {
        t.Fatalf("error when creating gogz Reader: %s\n", err)
    }

    cMember := reader.GetCurrentMember()
    if cMember == nil {
        t.Fatalf("current member is nil\n")
    }

    if cMember.Header.FileName != "rfc1952.txt" {
        t.Fatalf("invalid filename: [%s]\n", cMember.Header.FileName)
    }

    if cMember.Header.CompressionMethod != CmDeflate {
        t.Fatalf("invalid compression-method: %d\n", cMember.Header.CompressionMethod)
    }

    if cMember.Header.Os != OsUnix {
        t.Fatalf("invalid os %d\n", cMember.Header.Os)
    }

    defer reader.Close()
}
```

# Summary

In this part, I've created a boiler-plate code for the project. Defined all the
required data structures for processing of the _data member_ headers and written
some code to read the headers. In the next part, I'll have a look on the block
types within the _data member_ and we'll need some IO related abstractions in
order to have a bitwise input.

This concludes the first part of the series. All the code from this part in its
entirery can be found [here](https://gitlab.com/hesperos/gogz/tree/devdiary/part1).
