---
title: "Using mmap"
date: 2015-04-07T18:42:43Z
draft: false
tags: [ "mmap" ]
author: Tomasz Wisniewski (hesperos)
summary: This short example goes through a typical mmap usecase and the performance advantage it brings over standard file IO.
---

Out of curiosity I wanted to compare how much faster `mmap` file reading can be
when compared to the standard, "canonical" way we all know of reading files in
C. The standard way involves using `fread` style APIs and an intermediate
buffer to transfer data between file descriptors. This, of course, implies a
lot of copying. All of that potentially can be avoided when using `mmap` and
thus result in much better performance.

A word of introduction. `mmap` is a POSIX API which maps a "memory object" (a
shared memory, a file, etc) to the virtual memory space of the process.
What that means is that during standard file reading process there's no need to
read-in the contents from disk to a temporary buffer but instead the fragment
of the file is directly mapped to the memory space. In other words: there's no
intermediate copying involved. Refer to the man page for more details.

## The code
But how faster it really is? First of all we need a test program,
which looks the following way:


```
#define _FILE_OFFSET_BITS 64

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include <assert.h>

void mmap_file_cat(const char *a_fn, const size_t a_multiplier) {
    const size_t page_size = a_multiplier * sysconf(_SC_PAGESIZE);
    struct stat sbuff = {0x00};
    void *addr = NULL;
    int fd = 0;
    off_t offs = 0;
    size_t size = page_size;

    assert(a_fn);

    stat(a_fn, &sbuff);
    if (-1 == (fd = open(a_fn, O_RDONLY))) {
        perror("file open");
        return;
    }

    while(sbuff.st_size) {
        size = (page_size > sbuff.st_size) ? sbuff.st_size : page_size;
        addr = mmap(0, size, PROT_READ, MAP_PRIVATE, fd, offs);

        if (MAP_FAILED == addr) {
            close(fd);
            perror("mmap error");
            return;
        }

        sbuff.st_size -= size;
        offs += size;

        fwrite(addr, 1, size, stdout);
        munmap(addr, size);
    }

    close(fd);
}


void std_file_cat(const char *a_fn, const size_t a_multiplier) {
    const size_t page_size = a_multiplier * sysconf(_SC_PAGESIZE);
    char *buff = NULL;
    int fd = -1;
    ssize_t r = 0;

    assert(a_fn);

    if (-1 == (fd = open(a_fn, O_RDONLY))) {
        perror("file open");
        goto std_file_cat_exit;
    }

    if (NULL == (buff = malloc(page_size + 1) )) {
        perror("malloc");
        goto std_file_cat_exit;
    }

    while ((r = read(fd, buff, page_size))) {

        if (-1 == r) {
            perror("read");
            goto std_file_cat_exit;
        }

        buff[r] = '\0';
        fputs(buff, stdout);
    }

std_file_cat_exit:
    if (buff) free(buff);
    if (-1 != fd) close(fd);
}


int main(int argc, char *argv[])
{
    int i = 0;
    size_t multiplier = 0;

    if (argc <= 2) {
        fprintf(stderr, "mmap_cat <multiplier> <file1> <file2> ... <fileN>\n");
        return -1;
    }

    multiplier = strtoul(argv[1], NULL, 10);

    if (!multiplier) {
        fprintf(stderr, "Given Multiplier is not a number: [%s]\n", argv[1]);
        return -1;
    }

    for (i = 2; i < argc; i++) {
#ifdef MMAP_IO
        printf("[mmap]\n");
        mmap_file_cat(argv[i], multiplier);
#else
        printf("[std]\n");
        std_file_cat(argv[i], multiplier);
#endif
    }

    return 0;
}
```

It's a very simple standard Linux's "cat" command equivalent. There are two
main functions here:

- `std_file_cat()`
- `mmap_file_cat()`

The first one reads the file, chunk by chunk into an interim buffer and prints
the buffer to stdout until the end of file is reached. The second function uses
mmap to do the same thing. Since mmap operates on memory pages, the offsets in
the file must be multiples of the system page size.

I'll compile this code twice. Once with `-DMMAP_IO,` I'll call the resulting
binary `mmap_cat`, second one without, which I'll name `std_cat`.

The first argument which the program takes is the multiplier of the page size.
The intention is to measure if the buffer size (in case of `std_cat`) or the size
of mmaped memory has some significant impact on the performance. I'll perform
the tests with various values. The page size on my system is 4kB.

## Test scenario
The test program is pretty trivial. in essence it acts identical as "cat",
copies the file to stdout. I've decided to generate a big text file of
around 2 GB and "cat" it to /dev/null using both versions of test program.

The results are pretty consistent across the page size. Another thing is that
after the first run of every version some of the data is left cached which for
sure has a lot of influence.

The point of the whole test is simple. The average file copy time using
standard file IO is somewhere around 3 seconds. Copying the same file with mmap
takes around 0,01 second. `mmap()` is 300x times faster!

This example is really simple it's just going through the file in linear
fashion and reading some data. `mmap()` would really show it's power if we would
do a lot of small reads all over the file. If the problem you're coping with
requires that kind of scattered data access `mmap` should be the first thing in
your mind.
