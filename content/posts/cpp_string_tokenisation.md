---
title: "Tokenising a string in C++"
date: 2019-08-12T14:17:14+01:00
draft: false
tags: [ "c++", "c++11", "c++14" ]
author: Tomasz Wisniewski (hesperos)
summary: This is a quick note, describing ways to tokenise a string in c++.
---

# Space as delimiter

This is the simplest case. I.e:

```
std::string a = "string delimited with spaces";
std::stringstream ss(a);
std::vector<std::string> tokens{
    std::istream_iterator<std::string>(a),
    std::istream_iterator<std::string>()};
```

# Arbitrary character as delimiter

`std::getline` can be employed, allowing for custom character as delimiter:

```
std::string a = "04:5f:ab:3a:7c:3b:80";
std::vector<int> digits;
for (std::string token; std::getline(a, token, ':'); ) {
    int value = 0;
    std::istringstream(token) >> value;
    digits.push_back(value)

}
```

# Boost

Provides most flexibility - the downside is dependency on boost.

```
#include <iostream>
#include <string>
#include <vector>
#include <iterator>

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>

int main(int argc, const char *argv[])
{
    std::string a = "string delimited with spaces, and with commas";
    std::vector<std::string> tokens;
    boost::split(tokens, a, boost::is_any_of(", "));
    std::copy(tokens.begin(), tokens.end(), std::ostream_iterator<std::string>(std::cout, "\n"));

    return 0;
}

```
