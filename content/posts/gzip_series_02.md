---
title: "Writing gzip decompressor: Deflate fundamentals"
date: 2019-07-10T12:20:19+01:00
draft: false
tags: [ 'gzip', 'golang', 'compression', 'huffman' ]
author: Tomasz Wisniewski (hesperos)
summary: This is a part of a series where I describe my attempts to implement a gzip decompressor from scratch in golang. This part focuses on the fundamentals of the deflate compression algorithm
---

Understanding how deflate works is fundamental to understanding how modern
media is compressed. The algorithm used is very similar to how JPEGs are
compressed (after DCT transformation) as well. Same applies to MP3 and other
multimedia. Being able to decompress gzip is more or less a stepping stone to
decoding aforementioned other file formats.

[rfc1951.txt](https://www.ietf.org/rfc/rfc1951.txt) describes in details
everything that's going on. There are a couple of things worth outlining before
attempting to implement a decompression routines. I'll try to go through the
gotchas that the document doesn't cover in a great amount of details that, in
my opinion, require a word of explanation. But first, let's go through how
compressed data i structured within a _data member_.

# Block types

Data in a _data member_ is stored as a series of blocks. Each block has its
individual header (3 bits - yes, bits), denoting the block type and a flag
indicating if the block is a final block within the _data member_.

There are three block types:

* 00 - uncompressed
* 01 - compressed with fixed Huffman codes
* 02 - compressed with dynamic Huffman codes

## Uncompressed block

This one is very simple. On the next byte boundary there's another 4 bytes header
[rfc1951.txt:3.2.4](https://www.ietf.org/rfc/rfc1951.txt):


>      0   1   2   3   4...
    +---+---+---+---+================================+
    |  LEN  | NLEN  |... LEN bytes of literal data...|
    +---+---+---+---+================================+

>   LEN is the number of data bytes in the block.  NLEN is the
    one's complement of LEN.

## Block compressed with fixed Huffman codes

This block type is used most often when the overhead of including the Huffman
tables in the file would be bigger than the length of the data itself (which
compromises any efforts to minimise the length of the input sequence). Instead
of including the Huffman tables in the block, a fixed tables are assumed,
described in [rfc1951.txt:3.2.6](https://www.ietf.org/rfc/rfc1951.txt). There's
no additional header after the block header. Following 3-bit block header,
Huffman bit symbols representing the compressed data are expected.

## Block compressed with dynamic Huffman codes

This is the most interesting one. Following the block header, there's 14-bits
header describing the length of three alphabets (more on that later). Quoting
the rfc again:

>
    5 Bits: HLIT, # of Literal/Length codes - 257 (257 - 286)
    5 Bits: HDIST, # of Distance codes - 1        (1 - 32)
    4 Bits: HCLEN, # of Code Length codes - 4     (4 - 19)

The fun bit is that the alphabet for literal/length codes and distance codes is
itself Huffman compressed! So in order to get the details on Huffman codes for
Literal/Length and Distance Alphabets we need to decompress the alphabet that
is used to define them first.

This all starts to become quite complicated... before delving into more
details let's consider first what alphabets are, how they are being used
and how are they defined.

# Alphabets

The deflate algorithm is really a combination of LZ77 and Huffman coding. The
basic principle behind LZ77 is to replicate the repeating sequences of data in
the input streams by representing them using a pair of numbers:
<length,distance>. The length is the length of repeated sequence and the
distance is how far we have to move backwards in order to find its beginning.

Here's the example:

    "alpha bravo charlie alpha delta"
     --------+-------+-------+------
     0       8      16      24

The compressor may decide to represent the repeating sequence "alpha " (of
length 6) at position 20 with a pair of numbers <6,20>, effectively
producing 2 bytes on the output instead of 6. Of course, in order to determine
that this is a <length,distance> representation and not an ordinary data, the
sequence has to be either escaped somehow or use a dedicated alphabet. The
latter method is used in deflate algorithm. There are three distinct alphabets:

- literal bytes alphabet (0-255)
- length alphabet (3 - 258)
- distance alphabet (1 - 32768)

This hints that we have to maintain at least 32K of backwards lookup buffer in
order to replicate a sequence of up to 258 bytes long. Also the literal and
length alphabets are merged into a single alphabet 0..285. This alphabet
should be interpreted the following way:

- (0 - 255) - literal bytes
- 256 - indicates end-of-block
- (257 - 285) - represent length codes

The only remaining alphabet is the distance alphabet comprised of codes 0 - 29.

Note: For length and distance codes, it may be required to read some extra
bits in order to obtain the actual value.

# Huffman table representation

So, how are Huffman tables represented? The most naive representation would be
a direct association between a given symbol and it's Huffman code. This is not
the most efficient approach though and deflate comes with a clever way to
store Huffman tables in an efficient manner by adding some constraints to the
way Huffman codes are generated for given alphabet. This is all very nicely
described in [rfc1951.txt:3.2.2](https://www.ietf.org/rfc/rfc1951.txt). The
restrictions are simple:

- All codes of a given bit length have lexicographically consecutive values, in
the same order as the symbols they represent;

- Shorter codes lexicographically precede longer codes;

- Maximum possible code length in bits is 15.

This is the part which my opinion requires additional explanation as after
first glance, the algorithm generating the codes looks rather magical, while in
fact it's very simple.

The first rule dictates that ranges of codes of same length represent sorted
ranges of symbols from the original alphabet. I.e given that a fragment of
alphabet A, C, D, X, Y is encoded with codes of same length (which means that
the likeliness of any of these characters to occur in the input is
equal), the values for these codes will be consecutive:

- A   1000
- C   1001
- D   1010
- X   1011
- Y   1100

The second rule, defines how the values for the codes will be generated. It says that values for shorter codes will always be smaller than values for
longer codes. This restriction is there so that we can guarantee that resulting
codes will be *prefix codes*. What's a *prefix code*? This effectively means that
after reading a set of bits from the input stream we can unambiguously conclude
that given code represents a symbol and definitely will not represent any other
if we would consume more bits from the input stream. I.e:

Codes for this alphabet are not prefix codes:

- A   0
- B   1
- C   011
- D   010

After reading first bit (zero), we cannot conclude if it represents "A" or "C"
or "D". More input is required. If the next bit is _1_, we may be inclined to
think that the decoded symbol (01X) may be either C or D. Reading the last bit
doesn't resolve the ambiguoity either. Whatever it is one or zero it doesn't
matter. It the sequence is 011 it's impossible to tell if it represents "ABB"
or "C". Same with 010. It may be either "ABA" or "D".

Prefix codes solve that problem. For the same alphabet:

- A 10
- B 0
- C 110
- D 111

After reading first bit and checking that its value is _0_, it's possible to
unambigously conclude that it represents B. The decoding can proceed for the
next symbol - no back tracking required. Same applies for any code - once a
match is found any other longer (bitwise) code cannot start with the same
prefix. It's clearly visible in the example above, codes for symbols C, D,
cannot start with prefix _10_ or prefix _0_, the only prefix left is _11_.

Okay, but how are the tables stored? The represenation is very simple - only
the lengths of the symbols are stored. With the discussed constraints to
Huffman code generation this is the only required information in order to
recreate the original Huffman decoding tree. For the provided example:

- A 10
- B 0
- C 110
- D 111

The representation of the Huffman table will be simply: 2,1,3,3 - representing
used code lengths for the symbols from the original alphabet.



# Input/Output

Before we'll move forward with attempting to deal with any of aforementioned,
we need some IO routines. We need two things:

- read routine allowing to read the arbitrary number of bits from the input;

- a sliding window buffer (at least 32K) - in order to perform
lookups into already decoded output. This buffer acts like a fixed-size ring.
If more than 32K of data is written to it, exceeding data will be removed.


## BitReader

This object will allow to read input on a bit by bit basis. It will implement a
standard reader interface as well in order to provide a byte based access as
well. Here's the type definition:

```
type BitReader struct {
	r         io.Reader
	Available uint
	Data      uint64
}

```

and the most interesting function:

```
func (b *BitReader) ReadBits(n uint) (uint64, error) {
	var rv uint64 = 0
	if n > 64 {
		return 0, fmt.Errorf("unable to provide more than 64 bits in a single request\n")
	}

	for b.Available < n {
		var tmp uint8 = 0
		err := binary.Read(b.r, binary.LittleEndian, &tmp)
		if err != nil {
			switch err {
			case io.EOF:
				return 0, err
			default:
				return 0, fmt.Errorf("unable to cache data: %v\n", err)
			}
		}

		b.Data |= uint64(tmp) << b.Available
		b.Available += 8
	}

	rv = b.Data & ((0x01 << n) - 1)
	b.Data >>= n
	b.Available -= uint(n)
	return rv, nil
}
```

This code is very simple. It can provide up to 64 bits in one go - which
is more than enough, since the maximum possible code length is 15 bits in
case of deflate algorithm. It simply uses the `Data` buffer as a queue.
bits are pushed towards the most significant parts of the buffer and
popped from the least significant parts. Additionally, there's a bit
counter `Available` which indicates how many bits there are available. In
case number of bits available is smaller than requested - a new byte (or
more) will be obtained from the underlying reader.

This is everything requered in order to provide bitwise access.

## BackBuffer

In order to have an insight into already decoded output, I need something
that will maintain the history of the data (beyond client's buffers - as
client is allowed to do with them whatever they please to). A simple
wrapper around `bytes.Buffer` should do the trick. But first, I'm going
to create an interface for it:

```
type BackBufferReaderWriter interface {
	io.Reader
	io.ReaderAt
	io.Writer
}

```

The buffer implements a `Reader` interface - yes, since we want to access
it in a normal way. It implements the `ReaderAt` interface - that's in
order to have a way to random access required data in the maintained
history. It implements the writer interface as well - of course, since
I'll be writing into it the same time I'll be writing to client's
buffers.

Here's the type for which the mentioned interface will be implemented:

```
type BackBuffer struct {
	MaxSize int
	buffer  bytes.Buffer
}

```

Simple stuff, it's just a wrapper around `bytes.Buffer` with an extra
field indicating maximum size. Here's the `Write` function:

```
func (b *BackBuffer) Write(p []byte) (n int, err error) {
	n, err = b.buffer.Write(p)
	toStrip := b.buffer.Len() - b.MaxSize
	if toStrip > 0 {
		b.buffer.Truncate(toStrip)
	}
	return
}

```

After each `Write` operation, I just check the overal size of the buffer
- if it exceeded the configured maximum value, the difference will be
truncated. This implementation of course is not perfect as it allows for
**spikes** in the buffer, but to keep it simple, I'll leave it like that.

The `ReadAt` function provides a way to access a requested portion of the buffer at given offset:

```
func (b *BackBuffer) ReadAt(p []byte, off int64) (int, error) {
	bufLen := b.buffer.Len()
	if off >= int64(bufLen) {
		return 0, fmt.Errorf("ivalid offset %d, past buffer size: %d\n", off, bufLen)
	}
	return copy(p, b.buffer.Bytes()[off:]), nil
}

```

The `BackBuffer` of course, wraps some important operations of the
underlying `bytes.Buffer`.

# Changes to the code structure

In order to keep the naming convention alligned with other types, I've
renamed `gzd.go` -> `go_gz_reader.go`. Since the block decoding will
happen in the member implementation, the constructor function accepts
already described BackBufferReaderWriter interface as an argument.


# Summary

This summarises the required IO data types and operations in order to
conveniently read the data from the compressed member blocks and quickly
perform the LZ77 decoding. In the next part I'll focus on the program
structure and basic decoding of member blocks.

This concludes the first part of the series. All the code from this part in its
entirery can be found [here](https://gitlab.com/hesperos/gogz/tree/devdiary/part2).
