---
title: "Writing gzip decompressor: Uncompressed blocks"
date: 2019-08-13T13:44:47+01:00
draft: false
tags: [ "gzip", "deflate", "golang" ]
author: Tomasz Wisniewski (hesperos)
summary: This is a part of a series where I describe my attempts to implement a gzip decompressor from scratch in golang. This part focuses on implementing decompression of uncompressed blocks.
---

This is the last step towards a functionally complete implementation.
Reproduction of data from uncompressed blocks is relatively simple. There's a
short block header:

>
              0   1   2   3   4...
            +---+---+---+---+================================+
            |  LEN  | NLEN  |... LEN bytes of literal data...|
            +---+---+---+---+================================+

         LEN is the number of data bytes in the block.  NLEN is the
         one's complement of LEN.


The code responsible for this part is probably the simplest bit of the entire
implementation. The complete function is just a handful of lines and doesn't
really require any explanation:

```
func (m *Member) readUncompressedBlock() error {
	m.bitReader.Flush()
	var dataLen uint16 = 0
	var dataNLen uint16 = 0

	if err := binary.Read(m.bitReader, binary.LittleEndian, &dataLen); err != nil {
		return fmt.Errorf("unable to get the dataLen: %v\n", err)
	}
	if err := binary.Read(m.bitReader, binary.LittleEndian, &dataNLen); err != nil {
		return fmt.Errorf("unable to get the dataNLen: %v\n", err)
	}

	if dataLen != ^dataNLen {
		return fmt.Errorf("dataLen(%04x) != dataNLen(%04x)\n", dataLen, dataNLen)
	}

	io.CopyN(m.staging, m.bitReader, int64(dataLen))
	return nil
}

```

# Summary

That last bit functionally complements the gzip decoder implementation.

As usual, all the code from this part, in its
entirety can be found [here](https://gitlab.com/hesperos/gogz/tree/devdiary/part7).
