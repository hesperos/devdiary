---
title: "Implementing Hash tables from scratch in C++"
date: 2015-01-11T17:52:58Z
draft: false
tags: ["C++", "hash", "cpp"]
author: Tomasz Wisniewski (hesperos)
summary:
---

This is part two of the hash table post. In previous part I described the
basics behind implementation in C. Now, I'll re-implement the hash table in C++
using object oriented approach.

The whole implementation will be done in a dedicated namespace - ht (hash
table). The implementation will be able to cope with many different data types.
The key type can be of any type not only the strings.

```
namespace ht {

template <typename K, typename V>
class entry {
    K _key;
    V _value;

    template <typename KEY, typename VAL, unsigned N> friend class table;

public:
    entry() :
        _value(0)
    {
    }

    entry(const K& a_key) :
        _key(a_key),
        _value(0)
    {
    }

    void operator=(const V& a_value) {
#if DEBUG == 1
        std::cout << "set [" << _key << "]: " << a_value << std::endl;
#endif
        _value = a_value;
    }

    operator V() {
#if DEBUG == 1
        std::cout << "get [" << _key << "]" << std::endl;
#endif
        return _value;
    }

    virtual ~entry() = default;
};


template<typename K, typename H = void>
class hasher {
public:
    unsigned operator()(const K& a_key) const {
        return 0;
    }
};


template<>
class hasher<std::string> {
public:
    unsigned operator()(const std::string& a_key) const {
        unsigned int hash, i;
        for(hash = i = 0; i < a_key.length(); ++i) {
            hash += a_key[i];
            hash += (hash << 10);
            hash ^= (hash >> 6);

        }
        hash += (hash << 3);
        hash ^= (hash >> 11);
        hash += (hash << 15);
        return hash;
    }
};


template<typename K>
class hasher<K, typename std::enable_if<std::is_integral<K>::value >::type > {
public:
    unsigned operator()(const K& a_key) const {
        return a_key;
    }
};


template<typename K, typename V, unsigned N = 8192>
class table {

    using entry_t = entry<K, V>;
    using _bucket = std::forward_list<entry_t>;
    using _bucket_it = typename _bucket::const_iterator;
    using _bucket_details = std::tuple<_bucket_it, unsigned>;

    std::array<_bucket, N> _buckets;
    hasher<K> _hasher;
    unsigned int _collisions;
    unsigned int _elements;


    _bucket_details _find(const K& a_key) const {
        unsigned int i = _hasher(a_key) % N;
        auto it = std::find_if(_buckets[i].begin(),
                _buckets[i].end(),
                [&](const entry_t& b) -> bool { return b._key == a_key; });
        return std::make_tuple(it, i);
    }

public:
    table() :
        _collisions(0),
        _elements(0)
    {
    }


    virtual ~table() = default;


    bool exists(const K& a_key) {
        const _bucket_details& d = _find(a_key);
        return (std::get<0>(d) != _buckets[std::get<1>(d)].end());
    }


    entry_t& operator[](const K& a_key) {
        _bucket_it it;
        unsigned i = 0;

        std::tie(it, i) = _find(a_key);

        if (it == _buckets[i].end()) {
            // it doesn't exist

            // check if the bucket contains any elements,
            // if so, we have a collision
            if (!_buckets[i].empty()) _collisions++;
            _elements++;

            // push new element to the list
            _buckets[i].push_front(entry_t(a_key));
            it = _buckets[i].begin();
        }

        return const_cast< entry_t& >(*it);
    }


    void flush() {
        for (auto& b : _buckets) {
            b.clear();
        }

        _collisions = 0;
        _elements = 0;
    }


    unsigned size() const {
        return _elements;
    }


    unsigned collisions() const {
        return _collisions;
    }
};


} // namespace ht
```

Quick walk through the code. The hash table operates on entry<K,V> templates.
This is because the [] operator returns a reference to such entry which itself
is responsible for returning the encapsulated data.

The hashing function is implemented as a functor (an object overloading the ()
operator). This is a template as well. Depending on a key type this template is
specialized accordingly. I provided an implementation for strings (which is a
Bob Jenkins hash) and a specialization for any integral type. In such case I'm
simply returning the key value which is used directly as hash result. In order
to make this implementation complete more hashing functions would be needed for
pointers, compound types etc.

Most of the code in table itself is self explaining and practically identical
with the code written in C.

How to use it?


```
int main(int argc, char *argv[])
{
    ht::table<std::string, unsigned> t;
    ht::table<int, int> t2;
    int v = 0;
    constexpr unsigned max_data = 2048;

    t2[123] = 444;

    t["abc"] = 666;
    assert(t.exists("abc"));
    assert(!t.exists("bogus"));

    v = t["abc"];
    assert(v == 666 && "v == 666");

    v = t["bogus"];
    assert(!v && "getting bogus");

    assert(t.size() == 2);
    assert(t.collisions() == 0);

    v = 100;
    for (int i = 0; i < max_data; i++) {
        std::string s = "key_" + std::to_string(i);
        t[s] = v++;
    }
    assert(t.size() == (max_data + 2));

    v = 100;
    for (int i = 0; i < max_data; i++) {
        std::string s = "key_" + std::to_string(i);
        assert (t.exists(s));
        assert (t[s] == v++);
    }

    std::cout << "Number of collisions: " << t.collisions() << std::endl;
    return 0;
}
```

Summarizing. The implementation in C++ is more or less same in size as it's
equivalent in C. It's much more flexible though. Providing more opportunity to
use different data types as keys and define many different hashing functions
for each data type.

The complete code can be obtained [here](/devdiary/code/cpp_hash_table/hash.cpp).
