---
title: "Writing gzip decompressor: Checking data consistency"
date: 2019-07-22T13:10:37+01:00
draft: false
tags: [ 'gzip', 'golang', 'crc32', 'huffman' ]
author: Tomasz Wisniewski (hesperos)
summary: This is a part of a series where I describe my attempts to implement a gzip decompressor from scratch in golang. This part focuses on verifying data consistency after decompression.
---

This is a short addendum to the previous part, in which I'll add a data
consistency verification code. I'll calculate the Crc32 for all the data
decoded from a data member and confront the decoded data's length and the
checksum against what is provided in _data member's_ footer.

**Go** has a very nice mechanism to handle things like that: `io.MultiWriter`.
Since I'm decoding to the staging buffer first, all that has to be done is to
replace direct `Write` operations to that buffer with a `MultiWriter`.

# The Footer

The footer contains `crc32` of the original data, as well as its length modulo
2^32. In order to check if the decompressed output is consistent with the
original, its crc32 has to be recalculated and additionally the length should
match as well.

# Decompressed Length

The simplest way to handle this would be by simply having and incrementing the
length whenever a write occurs - but that's not an effortless approach I'm
after. In order to solve this in a _semi-automatic_ way, let's create a
dedicated trivial `io.Writer`:

```
type Counter struct {
	WrittenBytes uint
	ReadBytes    uint
}

func (c *Counter) Read(p []byte) (int, error) {
	c.ReadBytes += uint(len(p))
	return len(p), nil
}

func (c *Counter) Write(p []byte) (int, error) {
	c.WrittenBytes += uint(len(p))
	return len(p), nil
}

```

Very simple, it just tracks how much data has been written to it or read from
it. Doesn't do anything else. Let's add an instance of it to the Member
structure. It should now look like so:

```
type Member struct {
	Header *MemberHeader
	Footer *MemberFooter

	backBuffer    BackBufferReaderWriter
	staging       bytes.Buffer
	counter       Counter

	bitReader *BitReader
}
```

Let's leave it like that for now, I'll wire everything together in the next
step.

# Crc32

This is even easier. We've got Crc32 hashing function implementation provided
in _go's_ standard library. It just has to be used. Let's extend the `Member`
structure again but now additionally with a dedicated writer:

```

type Member struct {
	Header *MemberHeader
	Footer *MemberFooter

	backBuffer    BackBufferReaderWriter
	stagingBuffer bytes.Buffer
	staging       io.Writer
	counter       Counter
	crc32         hash.Hash32

	bitReader *BitReader
}

```

Quick overview. We've got a dedicated writer now called `staging` and the
actual `staging bytes.Buffer` has been renamed to `stagingBuffer`. Now, in
order to wire everything together, I'll take the advantage of the
aforementioned `io.MultiWriter`. In the `NewMember` constructor function:

```
	member := Member{
		Header:     header,
		backBuffer: b,
		bitReader:  bitReader,
		crc32:      crc32.NewIEEE(),
	}

	member.staging = io.MultiWriter(member.crc32,
		&member.stagingBuffer,
		&member.counter)
```

So, whenever a write occurs to `staging`, the data will be copied to three
destinations: `crc32`, `stagingBuffer` and `counter`. That's it. In the read
function whenever a block header indicates that I've read the final block of
the _data member_, I'll proceed with reading the footer and comparing it
against what I have in `crc32` and `counter`:

```

func (m *Member) VerifyFooter() error {
	if m.Footer.InputSize != uint32(m.counter.WrittenBytes) {
		return fmt.Errorf("length mismatch. decompressed: %d, expected: %d\n",
			uint32(m.counter.WrittenBytes),
			m.Footer.InputSize)
	}

	if m.Footer.Crc32 != m.crc32.Sum32() {
		return fmt.Errorf("crc32 mismatch: %x, expected: %x\n",
			m.crc32.Sum32(),
			m.Footer.Crc32)
	}

	return nil
}

...

func (m *Member) Read(p []byte) (int, error) {
    ...
		if isFinal {
			footer, err := NewMemberFooter(m.bitReader)
			if err != nil {
				return 0, err
			}
			m.Footer = footer
			err = m.VerifyFooter()
			if err != nil {
				return 0, err
			}
			break
		}
    ...
}

```

Already existing tests should still pass, since their checking the `Reader`
against a very simple data files. Let's add a negative test as well, which will
check for error in case of `crc32` inconsistency (for that I've prepared a file
with malformed footer):

```
func TestIfReportsCrcErrorsForCorruptedFiles(t *testing.T) {
	inputFh, err := os.Open("testdata/hw_corrupted_crc32.txt.gz")
	if err != nil {
		t.Fatalf("unable to open test asset: %s\n", err)
	}
	defer inputFh.Close()

	backBuffer, err := NewBackBuffer(123)
	if err != nil {
		t.Fatalf("unable to instantiate backbuffer: %s\n", err)
	}

	member, err := NewMember(inputFh, backBuffer)
	if err != nil {
		t.Fatalf("error when reading member: %s\n", err)
	}

	got := make([]byte, 1024)
	n, err := member.Read(got)
	if err == nil {
		t.Fatalf("expected crc error, got nothing\n")
	}

	if n != 0 {
		t.Fatalf("expected zero length read, got %d\n", n)
	}
}
```

# Summary

All the code from this part in its
entirery can be found [here](https://gitlab.com/hesperos/gogz/tree/devdiary/part4).

