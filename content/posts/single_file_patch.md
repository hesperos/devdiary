---
title: "Creating a patch for a single file from given git revision"
date: 2019-06-26T14:21:39+01:00
tags: [ "git" ]
author: Tomasz Wisniewski (hesperos)
summary: A quick way to generate a patch for selected files from a git commit.
---

When creating pull requests from development branches, sometimes there's a need
to select changes only for a single file from a given commit. A patch can be
very easily generated:

    git format-patch -1 <commit sha> <path>
