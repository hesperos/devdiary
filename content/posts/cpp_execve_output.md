---
title: "Catching the program output from execve system call"
date: 2014-12-15T16:59:20Z
draft: false
tags: [ "execve" ]
author: Tomasz Wisniewski (hesperos)
summary: Short summary of how to capture the stdout/stderr output from fork/exec'ed programs.
---

Executing an external command using system() or popen() will naturally under
Linux implicitly spawn a new shell instance and execute the requested command
under the shell's environment. Often that may be problem. If for some reason,
we have to process user data with an external command, spawning a shell
and possibly exposing it outside with our user interface may pose a great
security threat.

For that particular reason all of such calls should be executed as standalone
command without a wrapping shell session. But what if we need to capture the
command's output to a file? Under shell it's naturally done with the
redirections ">" for standard output (stdout) & "2>" for standard error output
(stderr). Without a shell instance this can't be done. The redirection symbols
mean nothing. The file descriptors must be set manually in order to do that.

Let's have a look at an example program:

```
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
 const char *cwd = ".";
 const char *dir = cwd;
 char cmd[128] = {0x00};

 if (argc >= 2) {
  dir = argv[1];
 }

 snprintf(cmd, sizeof(cmd), "ls -l %s", dir);
 system(cmd);
 return 0;
}
```

This program is trivial. In fact it's a kind of a wrapper for ls. This doesn't
really matter. The only important thing is that it gets an input from the user
(which is a directory) and passes that to system(). This is extremely
dangerous. Imagine what will happen if the user will provide the following
input:

    ./a.out ". && rm -rf /"


This will be happily passed to a shell instance spawned by `system()` and may
cause substantial damage. In order to prevent such action it's best to do such
calls without the underlying shell session, using `exec()` family of functions.
`exec()` functions replace the current process's text segment with the new one so
in result there is no way to go back to the calling process again. A call
through any `exec()` function must be done in a forked process in order to
simulate a `system()` function.

The modified program version looks the following way:

```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/wait.h>

extern char **environ;

int main(int argc, char *argv[]) {
 const char *cwd = ".";
 const char *dir = cwd;
 pid_t pid;

 if (argc >= 2) {
  dir = argv[1];
 }

 char * const cmd[] = { "/bin/ls", "-l", (char *)dir, NULL };

 pid = fork();
 if (-1 == pid) {
  // failure
  perror("fork");
  return -1;
 }
 else if (0 == pid) {
  // child
  execve("/bin/ls", cmd, environ);
  return 0;
 }

 // parent - wait's for the child to finish
 waitpid(pid, NULL, 0);

 return 0;
}
```

This version is safe. Trying to add something to the execution chain, will fail:

    ./a.out ". && echo test"
    /bin/ls: cannot access . && echo test: No such file or directory

But what if a redirection to a file and then, file parsing would be required?
There's no way to capture the output from the `execve()` call. In order to do
that, stream redirection must be done. This can be done with the `dup2()` system
call. The child processes inherit open file descriptors so, it can be done
before doing the `fork()` call.


```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/wait.h>

extern char **environ;

int main(int argc, char *argv[]) {
 const char *cwd = ".";
 const char *dir = cwd;
 pid_t pid;

 if (argc >= 2) {
  dir = argv[1];
 }

 char * const cmd[] = { "/bin/ls", "-l", (char *)dir, NULL };

 int stdout_log = 0;
 int stderr_log = 0;
 int stdout_org = 0;
 int stderr_org = 0;

 if (-1 == (stdout_log =
 open("stdout.log", O_RDWR | O_CREAT | O_APPEND, 0600))) {
  perror("stdout.log");
  return -1;
 }

 if (-1 == (stderr_log =
 open("stderr.log", O_RDWR | O_CREAT | O_APPEND, 0600))) {
  perror("stderr.log");
  return -1;
 }

 /*
  * dup2 will close the original
  * file descriptors, so copies
  * are needed in order to restore
  * the original stdout/stderr
  **/
 stdout_org = dup(fileno(stdout));
 stderr_org = dup(fileno(stderr));

 if (-1 == dup2(stdout_log, fileno(stdout))) {
  perror("stdout > stdout_log");
  return -1;
 }

 if (-1 == dup2(stderr_log, fileno(stderr))) {
  perror("stderr > stderr_log");
  return -1;
 }

 pid = fork();
 if (-1 == pid) {
  // failure
  perror("fork");
  return -1;
 } else if (0 == pid) {
  // child
  execve("/bin/ls", cmd, environ);
  return 0;
 }

 // parent - wait's for the child to finish
 waitpid(pid, NULL, 0);
 fflush(stdout);
 fflush(stderr);

 // restore original output
 dup2(stdout_org, fileno(stdout));
 dup2(stderr_org, fileno(stderr));

 close(stdout_log);
 close(stderr_log);
 puts("original output restored");

 return 0;
}
```

The code is longer, but it's not complex. The two `dup2()` calls at the beginning
are replacing currently opened stdout/stderr with the file descriptors of the
log files. The original file descriptors will be closed by the system call,
that's why a copy is made before that happens. The copy later on is used to
restore the original stdout/stderr descriptors at the end, so out program
may again print to the console.

The test program file can be downloaded from [here](/devdiary/code/cpp_execve_output/rs.c).
