---
title: "Building a custom AVR toolchain"
date: 2020-03-23T11:04:32Z
draft: false
tags: [ "avr", "gcc", "libc", "toolchain" ]
author: Tomasz Wisniewski (hesperos)
summary: This is a summary of steps I've taken in order to prepare a custom avr toolchain on macOSX
---

It turns out the the toolchain delivers through `brew` for avr devices (like
arduino), is not fully functional. Even with a simplest program I
had problems linking, as the compiler couldn't find a linker script for my
device. This seems like a gcc/libc incompatibility. The quickest way was to
prepare a custom toolchain on my own. Which is relatively easy, just
requires a bit of time to compile all the sources.

The general procedure of preparing any custom toolchain is pretty simple and
generic:

- prepare binutils (which provide ld, as, ar and all the basic set of initial
tools),
- prepare the compiler (gcc most likely),
- build libc of choice with your brand new compiler,
- optionally, build other supporting tools (gdb etc).


# Environment


    mkdir avr-gcc-env
    mkdir pkg src bld avr-gcc

As a first step, let's download everything:

    cd avr-gcc-env/pkg
    wget https://ftp.gnu.org/gnu/binutils/binutils-2.34.tar.xz
    wget https://ftp.gnu.org/gnu/gcc/gcc-9.3.0/gcc-9.3.0.tar.xz
    wget http://download.savannah.gnu.org/releases/avr-libc/avr-libc-2.0.0.tar.bz2

... unpack:

    cd ../src
    tar Jvxf ../pkg/binutils-2.34.tar.xz
    tar Jvxf ../pkg/gcc-9.3.0.tar.xz
    tar jvxf ../pkg/avr-libc-2.0.0.tar.bz2

Now, everything has to be built.


# Building `binutils`

    cd avr-gcc-env/bld
    mkdir binutils-2.34
    ../../src/binutils-2.34/configure \
        --prefix=$PWD/../../avr-gcc \
        --target avr && make && make install


# Building `gcc`


    cd avr-gcc-env/bld
    mkdir gcc-9.3.0
    cd gcc-9.3.0
    ../../src/gcc-9.3.0/configure \
        --prefix=$PWD/../../avr-gcc \
        --target avr \
        --enable-languages=c,c++ \
        --disable-nls && make && make install


# Building `libc`


    cd bld
    mkdir avr-libc-2.0.0
    cd avr-libc-2.0.0
    ../../src/avr-libc-2.0.0/configure \
        --prefix=$PWD/../../avr-gcc \
        --host avr && make && make install

`avr-gcc` now contains a ready to use toolchain. It can be added to PATH.
The rest of the directories can be removed.

Everything is summarised in a convenient script: [toolchain-builder](/devdiary/code/avr_toolchain/toolchain_builder.sh).
