---
title: "Simple transaction pattern"
date: 2020-02-09T18:51:43Z
draft: false
tags: ["c++", "locking", "wrapper"]
author: Tomasz Wisniewski (hesperos)
summary: A really neat pattern borrowed from one of Herb Sutter's talks
---

The pattern described by Herb allows to create wrappers around objects
that can decorate a group of calls performed on the object. I.e. it's not
always beneficial to perform locking on a per member function basis as
the granularity is to high and thus we're locking and unlocking
unnecessarily often. In such cases, this pattern is exactly what we want:

```
#include <iostream>
#include <thread>
#include <mutex>

template <typename T>
class Wrapper {
private:
    const T& t;
    mutable std::mutex mtx;

public:
    explicit Wrapper(const T& t) :
        t(t)
    {
    }

    template <typename FunctorT>
        auto operator()(FunctorT f) const -> decltype(f(t)) {
            std::lock_guard<std::mutex> l(mtx);
            return f(t);
        }
};

class X {
public:
    void foo() const {
        std::cout << "in foo" << std::endl;
    }

    void bar() const {
        std::cout << "in foo" << std::endl;
    }
};

int main(int argc, const char *argv[])
{
    X x;
    Wrapper<X> w(x);
    w([](const X& x) {
            x.foo();
            x.bar();
        });
    return 0;
}
```

The original talk can be found [here](https://channel9.msdn.com/Shows/Going+Deep/C-and-Beyond-2012-Herb-Sutter-Concurrency-and-Parallelism).
