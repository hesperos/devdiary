---
title: "Removing last argument, from function invocation"
date: 2017-05-28T16:28:39+01:00
draft: false
tags: [ "c++", "templates" ]
author: Tomasz Wisniewski (hesperos)
summary: This posts describes a simple technique to adapt existing function invocations to a new interface when the latter has been changed.
---

The argument list to a functor is being converted to a tuple. The tuple is then
unpacked in a template recursion but without the last argument. With this
technique it's possible to have some custom information appended to the
argument list. This extra argument won't be passed to the wrapped functor
itself but can provide an extra information about the call.


```
#include <iostream>
#include <string>
#include <memory>
#include <utility>
#include <functional>

struct MyFunctor {
    MyFunctor(int a, int b, int c) {
        std::cout << "MyFunctor called with args: " << a << ", " << b << ", " << c << std::endl;
    }
};

template <typename T, int N>
struct callWithNArgs {

    template <typename ... ARGST, typename ... ARGS>
    static std::shared_ptr<T> apply(const std::tuple<ARGST...>& t,
            ARGS ... args) {
        return callWithNArgs<T, N - 1>::apply(t,
                std::get<N - 1>(t), args...);
    }

};

template <typename T>
struct callWithNArgs<T, 0> {

    template <typename ... ARGST, typename ... ARGS>
    static std::shared_ptr<T> apply(const std::tuple<ARGST...>& t,
            ARGS ... args) {
        return std::make_shared<T>(args...);
    }
};

template <typename T, typename ... ARGS>
std::shared_ptr<T> callWrapper(ARGS&& ... args)
{
    std::tuple<ARGS...> a(args...);
    std::cout << "Called From: " << std::get<sizeof ... (args) - 1>(a) << std::endl;

    return callWithNArgs<T, sizeof...(args) - 1>::apply(a);
}

int main(int argc, char *argv[])
{
    auto p = callWrapper<MyFunctor>(1,2,3, __FUNCTION__);
    return 0;
}
```
