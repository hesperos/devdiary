---
title: "Unique sequential number generator"
date: 2016-04-16T17:02:07+01:00
draft: false
tags: ["c++", "boost"]
author: Tomasz Wisniewski (hesperos)
summary: This posts describes an approach to implement a sequential number generator. A similar implementation was used in production code. UUID was not used as it wasn't compatible with legacy implementation.
---

In an old code base I was dealing with a number generator class. It served as
an ID generator for one of the processes. The process itself is not important.
Recently there was a need to share this implementation with other process and
that's where the problems started. No synchronisation.

The class is pretty straight forward it opens a file reads the number,
increments it, writes the number back to a file and returns it to the client.
Simple stuff. It doesn't work though in multiprocess environment. In order to
make it work something like boost's `file_lock` is needed. Here's a full example:



```
template <typename T>
class NumGen {
    const std::string path;

    mutable std::mutex mtx;
    mutable boost::interprocess::file_lock ifl;

public:
    NumGen(const std::string& path) :
        path(path),
        ifl(path.c_str())
    {
    }


    T getNext() const
    {
        T current;

        // same process different threads lock
        std::lock_guard<std::mutex> l1(mtx);

        // interprocess lock
        boost::lock_guard<boost::interprocess::file_lock> l2(ifl);

        std::fstream f(path, std::fstream::in | std::fstream::out);

        if (!f.is_open()) {
            throw std::runtime_error("Unable to open: " + path);
        }

        f >> current;
        current++;

        f.seekg(0);
        f << current;

        f.flush();

        return current;
    }
};
```

A lock on `mutex` is needed to protect from concurrent access from the threads
of the same process and `file_lock` to lock out interprocess accesses.

A small test is required as well to check for collisions. The test program
generates half a million of IDs and that's it.



```
int main(int argc, char *argv[])
{
    IntGen g{ "index" };
    unsigned n = 500000;

    while(n--) {
        std::cout << g.getNext() << std::endl;
    }

    return 0;
}
```


To create processes I have this small shell script, which runs three processes
simultaneously and logs their output to respective log file, which is then
sorted. All files are checked for similarities. If there's none then it means
that the code works fine.


```
#!/bin/bash

echo 0 > index
seq 1 3 | parallel "./flock_tester > i{}.log"
ls i*log | parallel "sort {} > {.}.sorted"

n1=$(comm -12 i{1,2}.sorted | wc -l)
n2=$(comm -12 i{1,3}.sorted | wc -l)
n3=$(comm -12 i{2,3}.sorted | wc -l)

[ $(( n1 + n2 + n3 )) -eq 0 ] && echo "OK"
```

The whole bundle can be found for download
[here](https://drive.google.com/file/d/0ByE_WFvvg-guVzItSmt1THNVUFU/view). To
have a go, just compile it and run the shell script. [GNU
parallel](http://www.gnu.org/software/parallel/) is needed (and boost libraries
of course).
