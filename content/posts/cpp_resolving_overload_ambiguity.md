---
title: "Resolving overload ambiguity"
date: 2017-07-12T16:04:23+01:00
draft: false
tags: [ "c++", "boost" ]
author: Tomasz Wisniewski (hesperos)
summary: This post describes a way to force a certain function overload.
---

Binding an overloaded function can be difficult when the overload differs only
with CV qualifiers. In such cases, just as i.e.
[boost](http://www.boost.org/doc/libs/1_64_0/libs/bind/doc/html/bind.html#bind.troubleshooting.binding_an_overloaded_function)
documentation suggests,
the ambiguity should be resolved by an explicit cast to the requested
signature.


One situation where this is very common, is the get template function,
extracting Nth tuple value:


```
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>

#include <boost/tuple/tuple.hpp>
#include <boost/bind.hpp>

using T = boost::tuple<std::string, int>;


int main(int argc, const char *argv[])
{
    std::vector<T> pairs;

    pairs.emplace_back("test", 123);
    pairs.emplace_back("abc", 456);

    auto i = std::find_if(pairs.begin(), pairs.end(),
            boost::bind(
                static_cast<const int& (T::*)() const>(&T::get<1>),
                _1) == 123);

    if (i != pairs.end()) {
        std::cout << "found: " << boost::get<0>(*i) << std::endl;
    }
    else {
        std::cout << "not found" << std::endl;
    }
    return 0;
}
```


In order to make things simpler a type alias can be used:


```
using GetOne = const int& (T::*)() const;

auto i = std::find_if(pairs.begin(), pairs.end(),
        boost::bind(static_cast<GetOne>(&T::get<1>), _1) == 123);

```
