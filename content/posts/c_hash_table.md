---
title: "Implementing Hash tables from scratch in C"
date: 2015-01-15T17:33:03Z
draft: false
tags: ["hash", "c"]
author: Tomasz Wisniewski (hesperos)
summary: Implementing a simple hash table in C as an exercise.
---

Hash table is a very convenient and useful data structure. It's very handy for
implementing associative tables.

C comes with a rudimentary implementation available through `search.h`
header. There's a set of `hcreate`, `hsearch`, `hdestroy` APIs.

In C++ there's obviously the `std::map<>` container which implements the
associative table using the Red Black Tree. There's an
`std::unordered_set<>` and `std::unordered_map<>` as well which internally
are implemented as hash tables. So, as far as C++ is concerned there's no
real rationale to implement the hash table from scratch. Nevertheless
just for education and for practice (and for fun, of course) I'll do
that.

First of all, let's declare the data structures to operate on.


```
#define HT_MAX_SIZE 8191
#define HT_MAX_KEY_LENGTH 32


/**
 * @brief the type for the value
 */
typedef unsigned int value_t;


/**
 * @brief string to anything entry type
 */
struct ht_entry {
    char key[HT_MAX_KEY_LENGTH];
    value_t value;
};


/**
 * @brief linked list of entries
 */
struct ht_entry_list {
    struct ht_entry_list *next;
    struct ht_entry e;
};


/**
 * @brief the hash table. Contains an array of ht_entry_list linked lists
 */
struct ht {
    unsigned int collisions;
    unsigned int elements;
    struct ht_entry_list *entries[HT_MAX_SIZE];
};
```

My hash table will use strings of `HT_MAX_KEY_LENGTH` as keys and `unsigned int` as
values. The value type can be easily changed by modifying the `value_t` typedef.

Second of all, some interface is needed in order to operate on such defined
hash table. I'll define an empty placeholders which will be implemented
successively.

```
void ht_init(struct ht *ht) {
}

void ht_destroy(struct ht *ht) {
}

void ht_set(struct ht *ht,
        const char *key,
        value_t value) {
}

value_t ht_get(struct ht *ht, const char *key) {
}

unsigned int ht_exists(struct ht *ht,
        const char *key) {
}
```


Everything seems to be nice and clear I hope. Let's follow a simple TDD
approach and implement the easiest one `ht_init()` first.


```
int main(int argc, char *argv[]) {
    struct ht h;
    ht_init(&h);
    assert(h.elements == 0 && "number of elements");
    assert(h.collisions == 0 && "number of collisions");

    return 0;
}
```


This one can pass by accident, but most of the time will fail, since the
declaration of automatic variable does not initialize it implicitly. In order
to make it pass, the implementation of `ht_init()` should be:


```
void ht_init(struct ht *ht) {
    memset(ht, 0x00, sizeof(struct ht));
}
```

That was easy. Let's move on to something maybe more challenging. Obviously our
hash table will be useless if it won't be possible to store anything in it.
Let's write a simple test for storing the value and checking for it and provide
the implementation later on.


```
ht_set(&h, "abc", 123);
e = ht_exists(&h, "abc");
assert(e && "abc existence");
```


Oddly enough, this test will pass. That's because `ht_exists()` doesn't return
anything so we're left with some junk from the stack. Let's write another test:


    assert(!ht_exists(&h, "bogus") && "bogus");


The last one fails. Of course, before implementing any get/set function a
hashing function is needed, but before diving into a potential debugging hell,
let's keep it very simple:


```
unsigned int ht_hash(const char *key) {
    return 0;
}
```

For any given key, the hash function will always return zero. In result all the
data will be stored in the list of first entry, effectively rendering this
implementation into singly linked list. For the moment it's alright, that's
what I want.

If you think about the operations on the hash table then you'll notice that all
of them share the same code. For get/set/exist functions, that would be

- hash the key
- look for the key on the list of given entry

In order to save ourselves some code duplication let's implement a helper
function:

```
#define DEBUG 1

struct ht_ed {
    unsigned int i;
    struct ht_entry_list *e;
};

static struct ht_ed _ht_find_list_entry(struct ht *ht, const char *key) {
    unsigned int i = ht_hash(key);
    struct ht_entry_list *it = ht->entries[i];

    for (; it; it = it->next) {
        if (!strcmp(it->e.key, key)) {
            // the key already exists and it has been found
#if DEBUG == 1
            printf("the key [%s] found @[%d]\n", key, i);
#endif
            break;
        }
    }

    return (struct ht_ed){ .i = i, .e = it };
}
```

What's going on here ? I declared a new structure which I called `ht_ed`
(`hash_table` entry details :)). It stores an index in the entry table (generated
by the hashing function) and the pointer to the `ht_entry_list` (this may be `NULL`
if the entry with the searched key does not exist). At this stage we're more
than half way through. But in order to make our tests pass, let's implement the
`ht_set` & `ht_exist` operations. This will be quite simple since most of the code
has already been written in the `_ht_find_list_entry()` helper routine.

```
void ht_set(struct ht *ht,
        const char *key,
        value_t value) {
    struct ht_ed d = _ht_find_list_entry(ht, key);

    /* the entry doesn't yet exists */
    /* allocate new list node */
    if (NULL == d.e) {
        d.e = malloc(sizeof(struct ht_entry_list));
        memset(d.e, 0x00, sizeof(struct ht_entry_list));

        /* increment counters */
        ht->elements++;

        /* if list at that index already exists */
        /* that means we have a collision */
        if (ht->entries[d.i]) ht->collisions++;

        /* attach to the linked list */
        d.e->next = ht->entries[d.i];
        ht->entries[d.i] = d.e;

        /* copy the key */
        strncpy(d.e->e.key, key, sizeof(d.e->e.key));
    }

    /* set the value */
    d.e->e.value = value;
}


unsigned int ht_exists(struct ht *ht,
        const char *key) {
    struct ht_ed d = _ht_find_list_entry(ht, key);
    return d.e != NULL ? 1 : 0;
}
```

The set function uses the `_ht_find_list_entry()` helper to look up the entry. If
such doesn't exist a new one is created and added to the beginning of the list.
Our tests should pass now. Let's write a test for the `ht_get()` function now.


    v = ht_get(&h, "abc");
    assert(v == 123 && "v == 123");
    assert(!ht_get(&h, "bogus") && "getting bogus");


... and the code to make it pass:


```
value_t ht_get(struct ht *ht, const char *key) {
    struct ht_ed d = _ht_find_list_entry(ht, key);
    return d.e != NULL ? d.e->e.value : 0;
}
```


The tests are passing. It's time to implement a hashing function and see how
the table performs. Bob Jenkins's hashing routines are my favorites. I always
find them quite efficient and effective for my needs
[Jenkins hashing functions](http://en.wikipedia.org/wiki/Jenkins_hash_function).


```
unsigned int ht_hash(const char *key) {
    uint32_t hash, i;
    for(hash = i = 0; i < strlen(key); ++i) {
        hash += key[i];
        hash += (hash << 10);
        hash ^= (hash >> 6);

    }
    hash += (hash << 3);
    hash ^= (hash >> 11);
    hash += (hash << 15);
    return ( hash % HT_MAX_SIZE );
}
```


Having 8192 buckets let's verify how many collisions we'll have using that
implementation


```
#define MAX_ENTRIES 2048
v = 100;
for (c = 0; c < MAX_ENTRIES; c++) {
	sprintf(key, "key_%d", c);
	ht_set(&h, key, v++);
}

assert(h.elements == (MAX_ENTRIES + 1) && "number of elements after the loop");

v = 100;
for (c = 0; c < MAX_ENTRIES; c++) {
	sprintf(key, "key_%d", c);
	assert( ht_exists(&h, key) );
	assert( ht_get(&h, key) == v++ && "checking value");
}

printf("Number of collisions: %d\n", h.collisions);
```

The result is: 247. Which in my opinion is pretty decent. It's about 10% of the
whole data set.

As usual the complete code can be found [here](/devdiary/code/c_hash_table/hash.c).
