---
title: "Writing gzip decompressor: Implementing RunLength decompression"
date: 2019-07-22T17:45:38+01:00
draft: false
tags: [ 'gzip', 'golang', 'crc32', 'huffman' ]
author: Tomasz Wisniewski (hesperos)
summary: This is a part of a series where I describe my attempts to implement a gzip decompressor from scratch in golang. This part focuses on implementing run-length decompression and handling the alphabet symbols > 256.
---

It's time to add another bit of functionality and make this de-compressor more
complete :). This time I'll focus on handling the symbols representing the
`<length, distance>` tuples. These symbols are a fundamental concept for
run-length compression algorithms like LZW/LZ77. Deflate defines a variant of
this algorithm for its own use (mainly, in order to avoid any copyright
claims).

# Length & distance tables

In order to decode a `<length, distance>` pair, obviously the length and
distance is required in the first place. Obtaining this is easier than it
seems. It wasn't very clear for me initially but once I've delved into the code
it became completely clear. The
[rfc1951.txt:3.2.5](https://www.ietf.org/rfc/rfc1951.txt) defines two tables:
for interpreting the meaning of length symbols and distance symbols. This means
that after decoding a Huffman code, representing a symbol i.e. 266, its meaning
has to be checked with the length table. In this case 266 means that an extra
bit is required in order to determine the actual length. Let's read it. If it
occurs to be 0, then the length is 13, if it's 1, the length is 14. Easy! Since
it is already clear that we're in the middle of decoding a run-length sequence,
the next thing that has to be done is to read-in the distance value. There's
one gotcha here. It's not very clear but
[rfc1951.txt:3.2.6](https://www.ietf.org/rfc/rfc1951.txt) defines that for _fixed
blocks_, the length for distance codes is fixed and constant and it's always 5
bits. So, we need to read 5 bits from the stream and confront their value
against the distance table. I.e. if these occur to have a value of 16, the
table says that additional 7 bits are required. If after reading these occur to
have a value of ... let's say 15, then our distance value is: 16 + 15 = 31, so
the decoding distance pair would be `<length, distance> = <13, 31>`. This means
that a sequence of 13 bytes occurring 31 bytes backwards from current position
in the output buffer, has to be duplicated on the output.

Step by step the process looks like so:

1. Symbol > 256 -> start decoding run-length sequence
2. Obtain number of extra-bits from the length table to read from the stream.
3. Length = length[symbol] + extraBits
4. Read 5-bits from the input and confront their value with the distance table.
5. Obtain number of extra-bits from the distance table to read from the stream.
6. Distance = distance[symbol] + extraBits

## Overlapping sequences

There's one more gotcha to decoding these sequences. There's a special edge
case when the pair's `length > distance`. In such case, the sequence to be
re-emitted overlaps itself. I.e. if the pair's value is `<9, 4>` and the data
in the output, so far, is:


      alpha bravo charlie delta
      -+-------+-------+-------
     -24     -16      -8     -1

then, the sequence of length 4 **elta** has to be replicated twice and
additional **e** has to be produed on output in order to complete decoding of
that value pair. The output after decoding should look the following way:


      alpha bravo charlie deltaeltaeltae

Let's attempt to implement that. First of all the tables, as described in the
[rfc]() have to be represented somehow. I need to store the BaseValue,
representing either length or a distance, and an extra bits. The symbol
will be a key to the map:

```
type ExtraBits struct {
	ExtraBits uint8
	BaseValue uint16
}


type RunLengthAlphabet map[Symbol]ExtraBits
```

That's done. Now the tables themselves. As with the _fixed tables_ for
literal/length alphabet, I'll create a function returning them:

```
func getLengthExtraBits() RunLengthAlphabet {
	return RunLengthAlphabet{
		257: {0, 3},
		258: {0, 4},
		259: {0, 5},
		260: {0, 6},
		261: {0, 7},
		262: {0, 8},
		263: {0, 9},
		264: {0, 10},
		265: {1, 11},
		266: {1, 13},
		267: {1, 15},
		268: {1, 17},
		269: {2, 19},
		270: {2, 23},
		271: {2, 27},
		272: {2, 31},
		273: {3, 35},
		274: {3, 43},
		275: {3, 51},
		276: {3, 59},
		277: {4, 67},
		278: {4, 83},
		279: {4, 99},
		280: {4, 115},
		281: {5, 131},
		282: {5, 163},
		283: {5, 195},
		284: {5, 227},
		285: {0, 258},
	}
}

func getDistanceExtraBits() RunLengthAlphabet {
	return RunLengthAlphabet{
		0:  {0, 1},
		1:  {0, 2},
		2:  {0, 3},
		3:  {0, 4},
		4:  {1, 5},
		5:  {1, 7},
		6:  {2, 9},
		7:  {2, 13},
		8:  {3, 17},
		9:  {3, 25},
		10: {4, 33},
		11: {4, 49},
		12: {5, 65},
		13: {5, 97},
		14: {6, 129},
		15: {6, 193},
		16: {7, 257},
		17: {7, 385},
		18: {8, 513},
		19: {8, 769},
		20: {9, 1025},
		21: {9, 1537},
		22: {10, 2049},
		23: {10, 3073},
		24: {11, 4097},
		25: {11, 6145},
		26: {12, 8193},
		27: {12, 12289},
		28: {13, 16385},
		29: {13, 24577},
	}
}
```

Good! That's the tables. There's one more thing. I've mentioned that for _fixed
blocks_ the Huffman code-length is of fixed value of 5 bits for all symbols
representing the distances. If I represent that as a table (or a range) all of
the code used for generation of literal/length Huffman lookup tables can be re-
used. Let's do that:

```
func getFixedCodeDistanceSymbolRanges() []FixedCodeSymbolRange {
	// distance alphabet for fixed blocks is comprised of 5-bit codes
	return []FixedCodeSymbolRange{
		{0, 32, 5},
	}
}

```

The alphabet for distance values contains 32 symbols. Each symbol is
represented by 5 bit Huffman code. In order to use that range I have to
transform it, the same way as literal/length ranges:

```
codeList := convertCodeSymbolRangesToCodeList(
        getFixedCodeSymbolRanges())
lookup := GeneratHuffmanLookupFromCodeLengthList(codeList)
m.readCompressedBlock(lookup)
litLenCodeList := convertCodeSymbolRangesToCodeList(
        getFixedCodeLitLenSymbolRanges())
distanceCodeList := convertCodeSymbolRangesToCodeList(
        getFixedCodeDistanceSymbolRanges())

litLenLookup :=
        GeneratHuffmanLookupFromCodeLengthList(litLenCodeList)
distanceLookup :=
        GeneratHuffmanLookupFromCodeLengthList(distanceCodeList)

m.readCompressedBlock(litLenLookup, distanceLookup)

```

I've change the names for literal/length table from `lookup` to a more distinct
one, and modified the signature of `readCompressedBlock` to accept an extra
table for distance decoding. Let's have a look on the `readCompressedBlock`
function:

```
-func (m *Member) readCompressedBlock(hl HuffmanLookup) error {
-       minLen, maxLen := getMinMaxHuffmanCodeLength(hl)
+func (m *Member) readCompressedBlock(litLenLookup, distanceLookup HuffmanLookup) error {
+       llMinLen, llMaxLen := getMinMaxHuffmanCodeLength(litLenLookup)
+       distMinLen, distMaxLen := getMinMaxHuffmanCodeLength(distanceLookup)
        isEndOfBlock := false

        for !isEndOfBlock {
-               symbol, err := matchHuffmanCode(m.bitReader, hl, minLen, maxLen)
+               symbol, err := matchHuffmanCode(m.bitReader, litLenLookup, llMinLen, llMaxLen)
                if err != nil {
                        return err
                }
...
```

No biggy, In order to perform distance decoding I need the minimum and maximum
code-lengths. This looks a bit like an overkill now since, for distance codes
it is simply 5 ALL THE TIME. It is all right though. Keeping the function
generic like that will allow to approach _dynamic blocks_ decoding very
easily.

Let's approach decoding the `<length, distance>` pairs:

```
...
extraLenBits, ok := getLengthExtraBits()[symbol]
if !ok {
        return fmt.Errorf("unknown symbol - doesn't represent a length: [%x]\n", symbol)
}

extraLenBitsVal, err := m.bitReader.ReadBits(uint(extraLenBits.ExtraBits))
if err != nil {
        return err
}

// we've got the length to be repeated
length := extraLenBits.Value(extraLenBitsVal)

// proceed with obtaining information about the distance
symbol, err := matchHuffmanCode(m.bitReader, distanceLookup, distMinLen, distMaxLen)
if err != nil {
        return err
}

extraDistBits, ok := getDistanceExtraBits()[symbol]
if !ok {
        return fmt.Errorf("unknown symbol - doesn't represent a distance: [%x]\n", symbol)
}

extraDistBitsVal, err := m.bitReader.ReadBits(uint(extraDistBits.ExtraBits))
if err != nil {
        return err
}

// we've got the distance to look back to
distance := extraDistBits.Value(extraDistBitsVal)

...
```

This is pretty much as explained. Get number of extra bits from the length
table, read extra bits, determine the value for _length_. Decode Huffman code
for distance (5-bits) and get an associated symbol for it. Get number of extra
bits from the distance table. Read extra bits and determine the `distance`
value. Good, now I have a complete `<length, distance>` pair. It's time to use
the `backBuffer` and attempt to replicate the desired sequence on the output:

```
...

// obtain the data
repeats := make([]byte, length)
offset := int64(m.backBuffer.Len()) - int64(distance)
n, err := m.backBuffer.ReadAt(repeats, offset)
if err != nil {
	return fmt.Errorf("failed to perform backwards lookup: %v\n", err)
}
repeats = repeats[:n]

// replicate the data
for nRepeats := length / distance; nRepeats > 0; nRepeats-- {
	m.staging.Write(repeats)
}

if nRemaining := length % distance; nRemaining > 0 {
	repeats := make([]byte, nRemaining)
	n, err := m.backBuffer.ReadAt(repeats, offset)
	if int(nRemaining) != n || err != nil {
		return fmt.Errorf("failed to perform backwards lookup: %v\n", err)
	}
	m.staging.Write(repeats)
}
...
```

That's it! I need a test in order to verify that it works. Some test data is
required. Let's compress a trivial sequence:

	echoechoechodelta

The compressor, for sure, won't resist the temptation to replace the duplicated
`echo` with a look back sequence. Now, the test:

```
func TestIfIsAbleToReplicateRunLengthSequences(t *testing.T) {
	testRecords := []struct {
		GzFile string
	}{
		{"testdata/echo3.gz"},
	}

	for _, testRecord := range testRecords {
		inputFh, err := os.Open(testRecord.GzFile)
		if err != nil {
			t.Fatalf("unable to open test asset: %s\n", err)
		}
		defer inputFh.Close()

		// Decompress using production quality implementation in order to
		// obtain a reference data for comparison.
		expectedData, err := decompress(testRecord.GzFile)

		backBuffer, err := NewBackBuffer(32768)
		if err != nil {
			t.Fatalf("unable to instantiate backbuffer: %s\n", err)
		}

		member, err := NewMember(inputFh, backBuffer)
		if err != nil {
			t.Fatalf("error when reading member: %s\n", err)
		}

		gotData, err := ioutil.ReadAll(member)
		if err != nil {
			t.Fatalf("error when reading data: %s\n", err)
		}

		if len(expectedData) != len(gotData) {
			t.Fatalf("decompressed data length mismatch. got: %d, expected %d\n",
				len(gotData),
				len(expectedData))
		}

		for i, _ := range gotData {
			if gotData[i] != expectedData[i] {
				t.Fatalf("discrepant data @%d. got: [%x] expected: [%d]\n",
					i,
					gotData[i],
					expectedData[i])
			}
		}
	}

}
```

Simple stuff, read the file, with mine compressor and confront the output
against the output produced by a production quality implementation - they
should perfectly match. Let's run it!


```
--- FAIL: TestIfIsAbleToReplicateRunLengthSequences (0.00s)
panic: runtime error: slice bounds out of range [recovered]
	panic: runtime error: slice bounds out of range [recovered]
	panic: runtime error: slice bounds out of range

```

Hmm, it failed. It seems like there's out of bounds access to the `backBuffer`.
Well that has to be rectified. The problem is with the order of operations. The
backBuffer is populated in `GoGzReader's` `Read` function. So, it happens AFTER
we decompress the _data member_. I'm trying to perform a lookup in the back
buffer but it is essentially empty. In order to fix this, the `TeeReader` has
to be removed from `GoGzReader` and writing to `backBuffer` has to be done in
`MultiWriter` in member. After, adding this change... the test passes.


# Summary

All the ground work is there already. The next part is going to be most
interesting one - decompression of _dynamic blocks_. I'll be reading the actual
Huffman tables from the file and will be creating the custom lookup tables.

All the code from this part in its
entirety can be found [here](https://gitlab.com/hesperos/gogz/tree/devdiary/part5).

