---
title: "Writing gzip decompressor: Dynamically compressed blocks"
date: 2019-08-13T12:20:39+01:00
draft: false
tags: [ "gzip", "deflate", "golang" ]
author: Tomasz Wisniewski (hesperos)
summary: This is a part of a series where I describe my attempts to implement a gzip decompressor from scratch in golang. This part focuses on implementing run-length decompression and handling dynamically compressed blocks.
---

This is the most interesting bit in this entire exercise. The dynamic block
contains Huffman tables definition right after the block header and what's even
nicer is that already existing code is generic enough that no changes to the
decompression implementation are required - the only missing bit, is obtaining
the tables themselves.

# Reading the tables for literal/length and distance alphabets

Paragraph 3.2.7 of the [rfc1951.txt](https://www.ietf.org/rfc/rfc1951.txt)
describes the format of the block and how the tables are encoded. It's a bit
cryptic at first glance. Step by step the procedure is as follows:

>
    5 Bits: HLIT, # of Literal/Length codes - 257 (257 - 286)
    5 Bits: HDIST, # of Distance codes - 1        (1 - 32)
    4 Bits: HCLEN, # of Code Length codes - 4     (4 - 19)

The first number `hlit` tells how many literal/length symbols are used in total
to compress this block. This is a 5 bit number (0-31) that has to be enlared by
257.  The second 5 bit number `hdist` defines the total
number of distance codes required - this has to be increased by 1 - at least
one distance code will exist. The last number `hclen` describes the ...
number of codes? What does that even mean? As mentioned earlier, the Huffman
tables for literal/length and distance alphabet are compressed themselves! In
order to read these tables, they have to be decompressed. They are compressed
using a special fixed alphabet comprised of symbols from set `(0 .. 18>`. It's
perfectly plausible though that not all of these codes will be used - that's
why `hclen` is required. In order to decompress the literal/length, distance
tables I need to read either the entire 19 symbols for `(0..18>` alphabet or
`hclen` amount. The code-lengths for these symbols appear in a fixed
pre-defined order, described in rfc, which is:

```
   decompAlphaKeysOrder := []uint8{
       16, 17, 18, 0,
       8, 7, 9, 6,
       10, 5, 11, 4,
       12, 3, 13, 2,
       14, 1, 15,
}
```
At least 4 codes will always exist so, `hclen` itself has to be increased by
adding 4 to it. `hclen` code-lengths are of fixed 3-bits length.

Let's summarise all of that with some code:

```
+func readHuffmanTables(bitReader *BitReader) (HuffmanLookup, HuffmanLookup, error) {
+       // read the number of literal/length codes (257 - 286)
+       hlit, err := bitReader.ReadBits(5)
+       if err != nil {
+               return HuffmanLookup{}, HuffmanLookup{}, err
+       }
+
+       // read the number of distance codes (1 - 32)
+       hdist, err := bitReader.ReadBits(5)
+       if err != nil {
+               return HuffmanLookup{}, HuffmanLookup{}, err
+       }
+
+       // read the number of code lengths for the decompression alphabet (4 - 19)
+       hclen, err := bitReader.ReadBits(4)
+       if err != nil {
+               return HuffmanLookup{}, HuffmanLookup{}, err
+       }
+
+       hlit += 257
+       hdist += 1
+       hclen += 4
+
+       // Decompression alphabet contains symbols: 0 - 18
+       // Decompression alphabet is an alphabet used to compress
+       // the literal/length and distance Huffman code-lengths -
+       // in order to further save space. The literal/length and
+       // distance tables are recreated using a special
+       // algorithm as described in rfc1951.txt:3.2.7.
+       decompAlphaCodeLengthList := make([]uint8, 19)
+       decompAlphaKeysOrder := []uint8{
+               16, 17, 18, 0,
+               8, 7, 9, 6,
+               10, 5, 11, 4,
+               12, 3, 13, 2,
+               14, 1, 15,
+       }
+
+       // read the lengths of the Huffman codes used to encode
+       // the decompression alphabet
+       for i := 0; i < int(hclen); i++ {
+               codeLen, err := bitReader.ReadBits(3)
+               if err != nil {
+                       return HuffmanLookup{}, HuffmanLookup{}, err
+               }
+               decompAlphaCodeLengthList[decompAlphaKeysOrder[i]] = uint8(codeLen)
+       }
+
+       ...
+
}

```

Okay. I have the code-lengths for the table compression alphabet. What now? The
code to transform these into a lookup table already exists - it just has to be
used:

```
...
+       decompAlphaLookup := GeneratHuffmanLookupFromCodeLengthList(decompAlphaCodeLengthList)
+       minLen, maxLen := getMinMaxHuffmanCodeLength(decompAlphaLookup)
...
```

`decompAlphaLookup` is the Huffman lookup table that can be used to decompress
the literal/length, distance huffman tables, that should appear next in the
file. Now, it's a matter of matching `hlit + hdist` Huffman codes - in order to
construct the tables required to decompress the actual file contents. There's
another gotcha though. The decompression alphabet has a special meaning. As
already mentioned, the maximum code length for deflate algorithm is 15 bits.
Why would the alphabet be `(0..18>` then? Symbols `(16..18>` have a special
meaning. Quoting rfc:

>
               0 - 15: Represent code lengths of 0 - 15
                   16: Copy the previous code length 3 - 6 times.
                       The next 2 bits indicate repeat length
                             (0 = 3, ... , 3 = 6)
                          Example:  Codes 8, 16 (+2 bits 11),
                                    16 (+2 bits 10) will expand to
                                    12 code lengths of 8 (1 + 6 + 5)
                   17: Repeat a code length of 0 for 3 - 10 times.
                       (3 bits of length)
                   18: Repeat a code length of 0 for 11 - 138 times
                       (7 bits of length)

So, upon matching the code from `decompAlphaLookup` the value has to be
consulted with the above stipulated rules in order to recreate the set of
code-lengths defining the actual literal/length and distance tables:

```
+       huffmanCodeLenList := make([]uint8, 0, hlit+hdist)
+
+       for i := hlit + hdist; i > 0; {
+               symbol, err := matchHuffmanCode(bitReader, decompAlphaLookup, minLen, maxLen)
+               if err != nil {
+                       return HuffmanLookup{}, HuffmanLookup{}, err
+               }
+
+               var repeats uint64 = 0
+               var codeLen uint64 = 0
+
+               switch {
+               case symbol >= 0 && symbol <= 15:
+                       repeats = 1
+                       codeLen = uint64(symbol)
+
+               case symbol == 16:
+                       repeats, _ = bitReader.ReadBits(2)
+                       repeats += 3
+                       codeLen = uint64(huffmanCodeLenList[len(huffmanCodeLenList)-1])
+
+               case symbol == 17:
+                       repeats, _ = bitReader.ReadBits(3)
+                       repeats += 3
+
+               case symbol == 18:
+                       repeats, _ = bitReader.ReadBits(7)
+                       repeats += 11
+
+               default:
+               }
+
+               // fill in the code length list
+               for n := 0; n < int(repeats); n++ {
+                       huffmanCodeLenList = append(huffmanCodeLenList, uint8(codeLen))
+               }
+
+               i -= repeats
+       }
```

`huffmanCodeLenList` now contains a list of code lengths for both tables. The
range `(0..hlit)` contains code-lengths defining the literal/length table. The
remaining part from `hlit` onwards contains code-lengths defining the distance
table. This can be finally used to recreate the tables:

```
+       // generate the lookup tables
+       litLenLookup := GeneratHuffmanLookupFromCodeLengthList(huffmanCodeLenList[:hlit])
+       distanceLookup := GeneratHuffmanLookupFromCodeLengthList(huffmanCodeLenList[hlit:])

```

This feels a bit magical in some parts - but it is really pretty
straight-forward. I now have the tables that can be used to decompress the
dynamic block. They can be used like so:

```
func (m *Member) Read(p []byte) (int, error) {
...
		case BlockCompressedFixed:
			litLenCodeList := convertCodeSymbolRangesToCodeList(
				getFixedCodeLitLenSymbolRanges())
			distanceCodeList := convertCodeSymbolRangesToCodeList(
				getFixedCodeDistanceSymbolRanges())

			litLenLookup :=
				GeneratHuffmanLookupFromCodeLengthList(litLenCodeList)
			distanceLookup :=
				GeneratHuffmanLookupFromCodeLengthList(distanceCodeList)

			m.readCompressedBlock(litLenLookup, distanceLookup)

		case BlockCompressedDynamic:
			litLenLookup, distanceLookup, err := readHuffmanTables(m.bitReader)
			if err != nil {
				return 0, err
			}
			m.readCompressedBlock(litLenLookup, distanceLookup)
...
```

# Summary

This is all that's required to support dynamic blocks. There's one small bit
missing, required to be able to decompress the gzip file: support for
ucompressed blocks. This is what's going to be covered in the next part.

As usual, all the code from this part, in its
entirety can be found [here](https://gitlab.com/hesperos/gogz/tree/devdiary/part6).
