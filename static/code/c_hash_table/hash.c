#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdint.h>

#define DEBUG 0


#define HT_MAX_SIZE 8191
#define HT_MAX_KEY_LENGTH 32


/**
 * @brief the type for the value
 */
typedef unsigned int value_t;


/**
 * @brief string to anything entry type
 */
struct ht_entry {
	char key[HT_MAX_KEY_LENGTH];
	value_t value;
};


/**
 * @brief linked list of entries
 */
struct ht_entry_list {
	struct ht_entry_list *next;
	struct ht_entry e;
};


/**
 * @brief the hash table. Contains an array of ht_entry_list linked lists
 */
struct ht {
	unsigned int collisions;
	unsigned int elements;
	struct ht_entry_list *entries[HT_MAX_SIZE];
};


struct ht_ed {
	unsigned int i;
	struct ht_entry_list *e;
};


unsigned int ht_hash(const char *key) {
	uint32_t hash, i;
	for(hash = i = 0; i < strlen(key); ++i) {
		hash += key[i];
		hash += (hash << 10);
		hash ^= (hash >> 6);

	}
	hash += (hash << 3);
	hash ^= (hash >> 11);
	hash += (hash << 15);
	return ( hash % HT_MAX_SIZE );
}


void ht_init(struct ht *ht) {
	memset(ht, 0x00, sizeof(struct ht));
}


void ht_destroy(struct ht *ht) {
	for (unsigned int i = 0; i < HT_MAX_SIZE; i++) {
		struct ht_entry_list *p = ht->entries[i];
		while (p) {
			struct ht_entry_list *tmp = p;
			p = tmp->next;
			free(tmp);
		}
	}
}


static struct ht_ed _ht_find_list_entry(struct ht *ht, const char *key) {
	unsigned int i = ht_hash(key);
	struct ht_entry_list *it = ht->entries[i];

	for (; it; it = it->next) {
		if (!strcmp(it->e.key, key)) {
			// the key already exists and it has been found
#if DEBUG == 1
			printf("the key [%s] found @[%d]\n", key, i);
#endif
			break;
		}
	}

	return (struct ht_ed){ .i = i, .e = it };
}


void ht_set(struct ht *ht, const char *key, value_t value) {
	struct ht_ed d = _ht_find_list_entry(ht, key);

	/* the entry doesn't yet exists */
	/* allocate new list node */
	if (NULL == d.e) {
		d.e = malloc(sizeof(struct ht_entry_list));
		memset(d.e, 0x00, sizeof(struct ht_entry_list));

		/* increment counters */
		ht->elements++;

		/* if list at that index already exists */
		/* that means we have a collision */
		if (ht->entries[d.i]) ht->collisions++;

		/* attach to the linked list */
		d.e->next = ht->entries[d.i];
		ht->entries[d.i] = d.e;

		/* copy the key */
		strncpy(d.e->e.key, key, sizeof(d.e->e.key));
	}

	/* set the value */
	d.e->e.value = value;
}


value_t ht_get(struct ht *ht, const char *key) {
	struct ht_ed d = _ht_find_list_entry(ht, key);
	return d.e != NULL ? d.e->e.value : 0;
}


unsigned int ht_exists(struct ht *ht, const char *key) {
	struct ht_ed d = _ht_find_list_entry(ht, key);
	return d.e != NULL ? 1 : 0;
}


int main(int argc, char *argv[])
{
	struct ht h;
	int v = 0;
	int e = 0;
	int c = 0;
	char key[32] = {0x00};

	ht_init(&h);
	ht_set(&h, "abc", 123);
	e = ht_exists(&h, "abc");

	assert(e && "abc existence");
	assert(!ht_exists(&h, "bogus") && "bogus");

	v = ht_get(&h, "abc");
	assert(v == 123 && "v == 123");
	assert(!ht_get(&h, "bogus") && "getting bogus");

	assert(h.elements == 1 && "element count");
	assert(h.collisions == 0 && "element collisions");

#define MAX_ENTRIES 2048
	v = 100;
	for (c = 0; c < MAX_ENTRIES; c++) {
		sprintf(key, "key_%d", c);
		ht_set(&h, key, v++);
	}

	assert(h.elements == (MAX_ENTRIES + 1) && "number of elements after the loop");

	v = 100;
	for (c = 0; c < MAX_ENTRIES; c++) {
		sprintf(key, "key_%d", c);
		assert( ht_exists(&h, key) );
		assert( ht_get(&h, key) == v++ && "checking value");
	}

	printf("Number of collisions: %d\n", h.collisions);
	ht_destroy(&h);
	return 0;
}
