#!/bin/bash

CONCURRENT_JOBS=4

BINUTILS_VERSION="2.34"
GCC_VERSION="9.3.0"
LIBC_VERSION="2.0.0"

# prepare directories
mkdir pkg src bld avr-gcc

echo "downloading packages..."
(
    cd pkg
    wget https://ftp.gnu.org/gnu/binutils/binutils-${BINUTILS_VERSION}.tar.xz
    wget https://ftp.gnu.org/gnu/gcc/gcc-9.3.0/gcc-9.3.0.tar.xz
    wget http://download.savannah.gnu.org/releases/avr-libc/avr-libc-2.0.0.tar.bz2
)

echo "unpacking..."
(
    cd src
    tar Jvxf ../pkg/binutils-${BINUTILS_VERSION}.tar.xz
    tar Jvxf ../pkg/gcc-9.3.0.tar.xz
    tar jvxf ../pkg/avr-libc-2.0.0.tar.bz2
)

echo "building binutils"
(
    cd bld
    mkdir binutils-${BINUTILS_VERSION}
    cd binutils-${BINUTILS_VERSION}
    ../../src/binutils-${BINUTILS_VERSION}/configure \
        --prefix=$PWD/../../avr-gcc \
        --target avr && make -j ${CONCURRENT_JOBS} && make install
)

echo "building gcc"
(
    cd bld
    mkdir gcc-${GCC_VERSION}
    cd gcc-${GCC_VERSION}
    ../../src/gcc-${GCC_VERSION}/configure \
        --prefix=$PWD/../../avr-gcc \
        --target avr \
        --enable-languages=c,c++ \
        --disable-nls && make -j ${CONCURRENT_JOBS} && make install
)

echo "building libc"
(
    export PATH=$PWD/avr-gcc/bin:$PATH
    cd bld
    mkdir avr-libc-${LIBC_VERSION}
    cd avr-libc-${LIBC_VERSION}
    ../../src/avr-libc-${LIBC_VERSION}/configure \
        --prefix=$PWD/../../avr-gcc \
        --host avr && make -j ${CONCURRENT_JOBS} && make install
)
