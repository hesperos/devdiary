#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/wait.h>

extern char **environ;


int main(int argc, char *argv[]) {

	const char *cwd = ".";
	const char *dir = cwd;
	pid_t pid;

	if (argc >= 2) {
		dir = argv[1];
	}

	char * const cmd[] = { "/bin/ls", "-l", (char *)dir, NULL };

	int stdout_log = 0;
	int stderr_log = 0;
	int stdout_org = 0;
	int stderr_org = 0;

	if (-1 == (stdout_log = 
			open("stdout.log", O_RDWR | O_CREAT | O_APPEND, 0600))) {
		perror("stdout.log");
		return -1;
	}

	if (-1 == (stderr_log = 
			open("stderr.log", O_RDWR | O_CREAT | O_APPEND, 0600))) {
		perror("stderr.log");
		return -1;
	}

    /*
	 * dup2 will close the original
	 * file descriptors, so copies
	 * are needed in order to restore
	 * the original stdout/stderr
     */
	stdout_org = dup(fileno(stdout));
	stderr_org = dup(fileno(stderr));


	if (-1 == dup2(stdout_log, fileno(stdout))) {
		perror("stdout > stdout_log");
		return -1;
	}

	if (-1 == dup2(stderr_log, fileno(stderr))) {
		perror("stderr > stderr_log");
		return -1;
	}

	pid = fork();
	if (-1 == pid) {
		// failure
		perror("fork");
		return -1;
	}
	else if (0 == pid) {
		// child
		execve("/bin/ls", cmd, environ);
		return 0;
	}

	// parent - wait's for the child to finish
	waitpid(pid, NULL, 0);

	fflush(stdout);
	fflush(stderr);

	// restore original output
	dup2(stdout_org, fileno(stdout));
	dup2(stderr_org, fileno(stderr));

	close(stdout_log);
	close(stderr_log);

	puts("original output restored");

	return 0;
}
